<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['own'] = 'own';
$route['katalog/order'] = 'katalog/order';
//sementara
$route['ceo'] = 'ceo';
$route['personalia'] = 'personalia';
$route['admin'] = 'admin';
$route['operator'] = 'operator';
$route['worker'] = 'worker';
$route['iy'] = 'yasin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
