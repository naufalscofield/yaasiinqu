<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

class Home extends CI_Controller {

  public function __construct() {
	parent::__construct();
		$this->load->helper('url_helper');
    $this->load->model('ceo_model');
	}

  public function index() {
    $data['hasil'] = $this->ceo_model->get_hasil();
    $this->load->view('components/header');
    $this->load->view('home/index',$data);
    $this->load->view('components/footer');
  }


}
