<?php
defined('BASEPATH') OR exit('No Direct script access allowed');

class Order extends CI_Controller {

  public function __construct() {
	parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('insert_model');
	}
  //
  // public function insertas(){
  // 	$this->insert_model->insert_order();
  // 	redirect('order/pasca_order');
  // }


	public function insert(){
    // setting konfigurasi upload
    $config['upload_path'] = './assets/img/order';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '2000';
    // load library upload
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('foto')) {
        $error = $this->upload->display_errors();
        // menampilkan pesan error
        print_r($error);
    } else {
        $result = $this->upload->data();
        $name = $result['file_name'];
        $this->insert_model->insert_order($name);
        redirect('order/pasca_order');
    }
}

  public function pasca_order(){
    $this->load->view('order/pasca_order');
  }


}
