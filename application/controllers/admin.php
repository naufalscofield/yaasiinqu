<?php
  class admin extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');

      if($this->session->userdata('status') != "login"){
      redirect(base_url("login"));
      }
      $this->load->helper('url_helper');
      $this->load->model('admin_model');
      // $this->load->model('admin_model');
    }

    public function index(){
      $this->load->library('form_validation');

      $this->load->view('humans/admin/header');
      $this->load->view('humans/admin/sidebar');
      $this->load->view('humans/admin/profile');
      $this->load->view('humans/admin/footer');
    }

    public function update_profile(){
      $id_user = $this->input->post('id_user');
      $this->admin_model->update_profile($id_user);
      echo"<script>alert('Data profil berhasil di perbarui! Login kembali untuk validasi'); window.location = '../login/logout'</script>";
    }

    public function users($level=null){
      $data['user'] = $this->admin_model->get_user();
      $level = $this->input->post('level');

      if($level!=null){
        $data['user'] = $this->admin_model->get_user_level($level);
        $this->load->helper('form');

        $this->load->view('humans/admin/header');
        $this->load->view('humans/admin/sidebar');
        $this->load->view('humans/admin/users',$data);
        $this->load->view('humans/admin/footer');
      }else{
        $this->load->helper('form');

        $this->load->view('humans/admin/header');
        $this->load->view('humans/admin/sidebar');
        $this->load->view('humans/admin/users',$data);
        $this->load->view('humans/admin/footer');
      }
}

    public function delete_user($id_user){
      $this->admin_model->delete_user($id_user);
      redirect('admin/users');
    }

    public function insert_user(){

      $config['upload_path'] = './assets/img/users';
      $config['allowed_types'] = 'gif|jpg|png';
      // load library upload
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload('foto')) {
        $error = $this->upload->display_errors();
        // menampilkan pesan error
        print_r($error);
    } else {
      $result = $this->upload->data();
      $name = $result['file_name'];
      $this->admin_model->insert_user($name);
      // get naon?
  	   redirect('admin/users');
     }
    }

    public function hasil(){
        $data['hasil'] = $this->admin_model->get_hasil();

        $this->load->view('humans/admin/header');
        $this->load->view('humans/admin/sidebar');
        $this->load->view('humans/admin/hasil',$data);
        $this->load->view('humans/admin/footer');
      }

      public function delete_hasil($id_hasil){
        $this->admin_model->delete_hasil($id_hasil);
        redirect('admin/hasil');
    }

      public function insert_hasil(){

              $config['upload_path'] = './assets/img/hasil';
              $config['allowed_types'] = 'gif|jpg|png';
              $config['max_size'] = '100000';
              // load library upload
              $this->load->library('upload', $config);
               if (!$this->upload->do_upload('foto')) {
                 $error = $this->upload->display_errors();
              // menampilkan pesan error
               print_r($error);
                } else {
              $result = $this->upload->data();
              $name = $result['file_name'];
              $this->admin_model->insert_hasil($name);
              redirect('admin/hasil');
     }
 }

//==============================================================//
//==============================================================//
      public function katalog($id_desain=null){
        $data['cover'] = $this->admin_model->get_cover();
        $data['lembar'] = $this->admin_model->get_lembar();
        $data['kertas'] = $this->admin_model->get_kertas();
        $data['katalog'] = $this->admin_model->get_katalog();
        $data['data_katalog'] = [];

        if ($id_desain != null) {
        $data['data_katalog'] = $this->admin_model->get_katalog_id($id_desain);
        }
        $this->load->helper('form');
        $this->load->view('humans/admin/header');
        $this->load->view('humans/admin/sidebar');
        $this->load->view('humans/admin/katalog',$data);
        $this->load->view('humans/admin/footer');

  }

      public function delete_katalog($id_desain){
        $this->admin_model->delete_katalog($id_desain);
        redirect('admin/katalog');
}

    public function insert_katalog($id_desain=null){

        $config['upload_path'] = './assets/img/katalog';
        $config['allowed_types'] = 'gif|jpg|png';
        // load library upload
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
        $error = $this->upload->display_errors();
       // menampilkan pesan error
       print_r($error);
        } else {
       $result = $this->upload->data();
       $name = $result['file_name'];
       $this->admin_model->insert_katalog($name);
       redirect('admin/katalog');
     }
   }

    public function update_katalog(){
      $this->load->helper('form');

      $config['upload_path'] = './assets/img/katalog';
      $config['allowed_types'] = 'gif|jpg|png';
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('foto')) {
        $id_desain = $this->input->post('id_desain');
        $data = array(
          'kode_desain' => $this->input->post('kode_desain'),
          'id_cover' => $this->input->post('id_cover'),
          'id_lembar' => $this->input->post('id_lembar'),
          'id_kertas' => $this->input->post('id_kertas'),
          'warna' => $this->input->post('warna'),
          'harga' => $this->input->post('harga')
        );
      $this->admin_model->update_katalog($id_desain,$data);

    }else{

      $id_desain = $this->input->post('id_desain');
      $result = $this->upload->data();
      $foto = $result['file_name'];
      $data = array(
        'kode_desain' => $this->input->post('kode_desain'),
        'foto' => $foto,
        'id_cover' => $this->input->post('id_cover'),
        'id_lembar' => $this->input->post('id_lembar'),
        'id_kertas' => $this->input->post('id_kertas'),
        'warna' => $this->input->post('warna'),
        'harga' => $this->input->post('harga')
      );
    $this->admin_model->update_katalog($id_desain,$data);
    }
    redirect('admin/katalog');
 }

 public function update_status_katalog(){
   $id_desain = $this->input->post('id_desain');
   $status = $this->input->post('status');
   // print_r($id_desain); die;
   //
   $data = array(
     'status' => $status
   );
   $this->admin_model->update_status_katalog($id_desain,$data);
   redirect('admin/katalog');
 }

 //===================================================//
 //===================================================//

    public function payment($id_bank=null){
      $data['payment'] = $this->admin_model->get_payment();
      $data['data_payment'] = [];

      if($id_bank != null){
      $data['data_payment'] = $this->admin_model->get_payment_id($id_bank);
      $this->load->helper('form');
      $this->load->view('humans/admin/header');
      $this->load->view('humans/admin/sidebar');
      $this->load->view('humans/admin/payment',$data);
      $this->load->view('humans/admin/footer');

      }else{

      $this->load->view('humans/admin/header');
      $this->load->view('humans/admin/sidebar');
      $this->load->view('humans/admin/payment',$data);
      $this->load->view('humans/admin/footer');

    }
 }

    public function delete_payment($id_bank){
      $this->admin_model->delete_payment($id_bank);
      redirect('admin/payment');
}

    public function insert_payment(){
      $this->admin_model->insert_payment();
      redirect('admin/payment');
    }

    public function update_payment($id_bank){
      $this->admin_model->update_payment($id_bank);
      redirect('admin/payment');
    }

//=====================================================================//

  public function materials(){

     $data['cover'] = $this->admin_model->getcover();
     $data['kertas'] = $this->admin_model->getkertas();
     $data['lembar'] = $this->admin_model->getlembar();

     $this->load->helper('form');

     $this->load->view('humans/admin/header');
     $this->load->view('humans/admin/sidebar');
     $this->load->view('humans/admin/material',$data);
     $this->load->view('humans/admin/footer');
   }

  public function insert_cover(){
       $this->admin_model->insert_cover();
       redirect('admin/materials');
  }

  public function insert_paper(){
    $this->admin_model->insert_paper();
    redirect('admin/materials');
  }

  public function insert_pages(){

    $this->admin_model->insert_pages();
    redirect('admin/materials');
    }

  public function delete_cover($id_cover){
    $this->admin_model->delete_cover($id_cover);
    redirect('admin/materials');
  }

  public function delete_paper($id_kertas){
    $this->admin_model->delete_paper($id_kertas);
    redirect('admin/materials');
  }

  public function delete_pages($id_lembar){
    $this->admin_model->delete_pages($id_lembar);
    redirect('admin/materials');
  }

  public function city($id_kota=null){
    $data['kota'] = $this->admin_model->get_kota();
    $data['data_kota'] = [];

    if($id_kota != null){

    $data['data_kota'] = $this->admin_model->get_kota_id($id_kota);
    // print_r($data['data_kota']); die;
      // print_r($data['data_kota']); die;
    $this->load->helper('form');
    $this->load->view('humans/admin/header');
    $this->load->view('humans/admin/sidebar');
    $this->load->view('humans/admin/kota',$data);
    $this->load->view('humans/admin/footer');

    }else{

    $this->load->view('humans/admin/header');
    $this->load->view('humans/admin/sidebar');
    $this->load->view('humans/admin/kota',$data);
    $this->load->view('humans/admin/footer');

  }
}

  public function insert_city(){
    $this->admin_model->insert_kota();
    redirect('admin/city');
  }

  public function delete_city($id_kota){
    $this->admin_model->delete_kota($id_kota);
    redirect('admin/city');
  }

  public function update_city(){
    $this->admin_model->update_kota();
    redirect('admin/city');
  }
}
?>
