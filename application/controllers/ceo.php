<?php
  class ceo extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');

      if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		  }
      $this->load->helper('url_helper');
      $this->load->model('ceo_model');
    }

    public function index(){


      //$data['orderan'] = $this->ceo_model->get_profile();

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/profile');
      $this->load->view('humans/ceo/footer');
    }

    public function update_profile(){

        $id_user = $this->input->post('id_user');
        $this->ceo_model->update_profile($id_user);
        echo"<script>alert('Data profil berhasil di perbarui! Login kembali untuk validasi'); window.location = '../login/logout'</script>";

    }

    public function orders($status=null){

      $status = $this->input->post('status');

      if($status!=null){
        $data['orderan'] = $this->ceo_model->get_order_id($status);

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/orders',$data);
        $this->load->view('humans/ceo/footer');
    }else{
      $data['orderan'] = $this->ceo_model->get_order();

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/orders',$data);
      $this->load->view('humans/ceo/footer');
    }
  }
    public function orders_date($tanggal=null){

      $tanggal = $this->input->post('tanggal');

      if($tanggal!=null){
        $data['orderan'] = $this->ceo_model->get_order_tanggal($tanggal);

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/orders',$data);
        $this->load->view('humans/ceo/footer');
    }else{
      $data['orderan'] = $this->ceo_model->get_order();

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/orders',$data);
      $this->load->view('humans/ceo/footer');
    }
  }

    public function delete_order($id_order){
      $this->ceo_model->delete_order($id_order);
      redirect('ceo/orders');
    }
//=====================================================================//
    public function users($level=null){
      $level = $this->input->post('level');
      $data['user'] = $this->ceo_model->get_user();

      if($level != null){
        //jika ada level yang dipilih
        $data['user'] = $this->ceo_model->get_user_level($level);

        $this->load->helper('form');

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/users',$data);
        $this->load->view('humans/ceo/footer');
      }else{
        //jika ada level yang dipilih namun tidak ada id_user(aksi update levelup)
        $data['user'] = $this->ceo_model->get_user();

        $this->load->helper('form');

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/users',$data);
        $this->load->view('humans/ceo/footer');
       }
     }

    public function delete_user($id_user){
      $this->ceo_model->delete_user($id_user);
      redirect('ceo/users');
    }

    public function update_user(){
       // print_r($id);
       // print_r($levelup); die;
       $this->ceo_model->update_user();
       redirect('ceo/users');
    }

//===========================================================//

    public function catalogues($id_cover=null){
      $data['katalog'] = $this->ceo_model->get_katalog();
      $id_cover = $this->input->post('id');
      if ($id_cover != null){
        $data['katalog'] = $this->ceo_model->get_katalog_where($id_cover);
        $this->load->helper('form');

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/katalog',$data);
        $this->load->view('humans/ceo/footer');
      }else{


      $this->load->helper('form');

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/katalog',$data);
      $this->load->view('humans/ceo/footer');
    }
    }

//===========================================================//

    public function examples(){
      $data['hasil'] = $this->ceo_model->get_hasil();

      $this->load->helper('form');

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/hasil',$data);
      $this->load->view('humans/ceo/footer');
    }

//===========================================================//

    public function materials(){
      $data['cover'] = $this->ceo_model->get_cover();
      $data['kertas'] = $this->ceo_model->get_kertas();
      $data['lembar'] = $this->ceo_model->get_lembar();

      $this->load->helper('form');

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/material',$data);
      $this->load->view('humans/ceo/footer');
    }

//===========================================================//

    public function balances($id_bank=null){
      $data['balance'] = $this->ceo_model->get_balance();

      $data['data_balance'] = [];
        if($id_bank != null){
      $data['data_balance'] = $this->ceo_model->get_balance_id($id_bank);
      }
      $this->load->helper('form');

      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/balances',$data);
      $this->load->view('humans/ceo/footer');
    }
    //===========================================================//

    public function insert_balance(){

          $this->ceo_model->insert_balance();
          redirect('ceo/balances');
      // }

  }

    public function update_balance($id_bank){
      $this->load->library('form_validation');

       $this->ceo_model->update_balance($id_bank);
       redirect('ceo/balances');
     }

     public function delete_balance($id_bank){
       $this->ceo_model->delete_balance($id_bank);
       redirect('ceo/balances');
     }
     //=================================================================//
     public function graph(){

        // $data['cover'] = $this->ceo_model->get_cover();
        $data['orderan'] = $this->ceo_model->get_order2();
        // $counts = array_count_values($data['orderan']);
        // print_r($count['id_cover']);die;
        // print_r($data['orderan']);die;

        $this->load->view('humans/ceo/header');
        $this->load->view('humans/ceo/sidebar');
        $this->load->view('humans/ceo/graph',$data);
        $this->load->view('humans/ceo/footer');
    }

    public function city(){
      $data['kota'] = $this->ceo_model->get_kota();


      $this->load->helper('form');
      $this->load->view('humans/ceo/header');
      $this->load->view('humans/ceo/sidebar');
      $this->load->view('humans/ceo/kota',$data);
      $this->load->view('humans/ceo/footer');

    }
  }
?>
<!-- public function users($id_user=null){
  $data['user'] = $this->ceo_model->get_user();

  $data['data_user'] = [];
  if ($id_user != null) {
  $data['data_user'] = $this->ceo_model->get_user_id($id_user);
 }

  $this->load->helper('form');

  $this->load->view('humans/ceo/header');
  $this->load->view('humans/ceo/sidebar');
  $this->load->view('humans/ceo/users',$data);
  $this->load->view('humans/ceo/footer');
} -->
