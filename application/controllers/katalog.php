<?php
class katalog extends ci_controller{

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
    //me load helper CI bernama url helper
    $this->load->model('katalog_model');
    $this->load->model('payment_model');
    $this->load->model('kota_model');
    //me load model bernama katalog_model dan payment model
  }

  public function index($id=null){
    $id = $this->input->post('cover');
    // print_r($id); die;
    if($id != null){
      $data['katalog'] = $this->katalog_model->get_katalog_where($id);

      $this->load->view('components/header');
      $this->load->view('katalog/index',$data);
      $this->load->view('components/footer');
    }else{
      $data['katalog'] = $this->katalog_model->get_katalog();

      $this->load->view('components/header');
      $this->load->view('katalog/index',$data);
      $this->load->view('components/footer');
    }
  }

  public function order($id=null){
    $id = $this->input->post('id_desain');
    if($id!=null){
      //MENANGKAP HASIL POST DARI PAGE SEBELUMNYA

      $data['payment'] = $this->payment_model->get_bank();
      $data['kota'] = $this->kota_model->get_kota();
      $data['warna'] = explode(',',$this->katalog_model->get_color($id));
      $data['kode_desain'] = $this->input->post('kode_desain');
      $data['harga'] = $this->input->post('harga');
      $data['id_cover'] = $this->input->post('id_cover');
      $data['foto'] = $this->input->post('foto');

      // print_r($data['[payment]']); die;
      // print_r($data['warna']); die;

      $this->load->view('components/header');
      $this->load->view('order/index', $data);
      $this->load->view('components/footer');
    }else{
      redirect('katalog');
  }
}
}
?>
