<?php
class login extends CI_Controller{

  public function __construct(){
  parent::__construct();
  $this->load->helper('url_helper');
  $this->load->model('loginku');
  $this->load->library('session');
}

  public function index(){
  $this->load->view('login/index');
}

  public function aksi_login(){
		$data = array(
            'username' => $this->input->post('username'),
						'password' => md5($this->input->post('password'))
			       );
    // $password = $this->input->post('password');
		$hasil = $this->loginku->cek_login($data);
		if ($hasil->num_rows() == 1) {
			   foreach ($hasil->result() as $sess) {
        // $password = $this->input->post('password');
        // print_r($password); die;
        $sess_data['id_user'] = $sess->id_user;
        $sess_data['nama_depan'] = $sess->nama_depan;
        $sess_data['nama_belakang'] = $sess->nama_belakang;
				$sess_data['username'] = $sess->username;
				$sess_data['password'] = $sess->password;
				$sess_data['alamat'] = $sess->alamat;
				$sess_data['no_hp'] = $sess->no_hp;
				$sess_data['email'] = $sess->email;
				$sess_data['foto'] = $sess->foto;
				$sess_data['level'] = $sess->level;
				$sess_data['status'] = 'login';
				$sess_data['pw'] = $this->input->post('password');
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('level')=='CEO') {
        redirect('ceo');
			}
      elseif ($this->session->userdata('level')=='PERSONALIA') {
        redirect('personalia');
		  }
      elseif ($this->session->userdata('level')=='ADMIN') {
      redirect('admin');
      }
      elseif ($this->session->userdata('level')=='OPERATOR') {
      redirect('operator');
      }
      elseif ($this->session->userdata('level')=='WORKER') {
      redirect('worker');
      }
    }
      else
      echo"<script>alert('Username atau Password anda salah!'); window.location = '../login'</script>";
}
  public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
?>
