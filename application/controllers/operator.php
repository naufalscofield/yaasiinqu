<?php
  class operator extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');

      if($this->session->userdata('status') != "login"){
      redirect(base_url("login"));
      }
      $this->load->helper('url_helper');
      $this->load->model('operator_model');
    }

    public function index(){
      $this->load->library('form_validation');

      $this->load->view('humans/operator/header');
      $this->load->view('humans/operator/sidebar');
      $this->load->view('humans/operator/profile');
      $this->load->view('humans/operator/footer');
    }

    public function update_profile(){
        $id_user = $this->input->post('id_user');
        $this->operator_model->update_profile($id_user);
        echo"<script>alert('Data profil berhasil di perbarui! Login kembali untuk validasi'); window.location = '../login/logout'</script>";
    }


    public function orders($status=null,$kode=null){
      $status = $this->input->post('status');
      $kode = $this->input->post('kode_order');

      if($status!=null){
        $data['orderan'] = $this->operator_model->get_order_id($status);
        $this->load->view('humans/operator/header');
        $this->load->view('humans/operator/sidebar');
        $this->load->view('humans/operator/orders',$data);
        $this->load->view('humans/operator/footer');
      }else{

        $data['orderan'] = $this->operator_model->get_order();

        $this->load->view('humans/operator/header');
        $this->load->view('humans/operator/sidebar');
        $this->load->view('humans/operator/orders',$data);
        $this->load->view('humans/operator/footer');
      }
    }

    public function orders_code($kode=null){
      $kode = $this->input->post('kode_order');

      if($kode!=null){
        $data['orderan'] = $this->operator_model->get_order_kode($kode);
        $this->load->view('humans/operator/header');
        $this->load->view('humans/operator/sidebar');
        $this->load->view('humans/operator/orders',$data);
        $this->load->view('humans/operator/footer');
      }else{

        $data['orderan'] = $this->operator_model->get_order();

        $this->load->view('humans/operator/header');
        $this->load->view('humans/operator/sidebar');
        $this->load->view('humans/operator/orders',$data);
        $this->load->view('humans/operator/footer');
      }
    }

    public function delete_order($id_order){
      $this->operator_model->delete_order($id_order);
      redirect('operator/orders');
    }

    public function update_order($id_order){
      $this->operator_model->update_order($id_order,'proccess');
      redirect('operator/orders');
    }

    public function orders_proccess(){
     $data['orderan'] = $this->operator_model->get_order_proccess();

     $this->load->view('humans/operator/header');
     $this->load->view('humans/operator/sidebar');
     $this->load->view('humans/operator/orders_proccess',$data);
     $this->load->view('humans/operator/footer');

   }

   public function update_order_proccess($id_order){
     $this->operator_model->update_order_proccess($id_order,'done');
     redirect('operator/orders_proccess');
   }
  }
?>
