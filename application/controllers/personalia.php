<?php
  class personalia extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');

      if($this->session->userdata('status') != "login"){
      redirect(base_url("login"));
      }
      $this->load->helper('url_helper');
      $this->load->model('personalia_model');
    }

    public function index(){
      // $password = $this->input->post('password');
      //$data['orderan'] = $this->personalia_model->get_profile();
//fungsi get na mana
      $this->load->view('humans/personalia/header');
      $this->load->view('humans/personalia/sidebar');
      $this->load->view('humans/personalia/profile');
      $this->load->view('humans/personalia/footer');
    }

    public function update_profile(){
        $id_user = $this->input->post('id_user');
        $this->personalia_model->update_profile($id_user);
        echo"<script>alert('Data profil berhasil di perbarui! Login kembali untuk validasi'); window.location = '../login/logout'</script>";
    }

    public function users($level=null){
      $data['user'] = $this->personalia_model->get_user();
      $level = $this->input->post('level');

      if($level!=null){
        $data['user'] = $this->personalia_model->get_user_level($level);
        $this->load->helper('form');

        $this->load->view('humans/personalia/header');
        $this->load->view('humans/personalia/sidebar');
        $this->load->view('humans/personalia/users',$data);
        $this->load->view('humans/personalia/footer');
      }else{
        $this->load->helper('form');

        $this->load->view('humans/personalia/header');
        $this->load->view('humans/personalia/sidebar');
        $this->load->view('humans/personalia/users',$data);
        $this->load->view('humans/personalia/footer');
      }
}

    public function delete_user($id_user){
      $this->personalia_model->delete_user($id_user);
      redirect('personalia/users');
    }

    public function insert_user(){

      $config['upload_path'] = './assets/img/users';
      $config['allowed_types'] = 'gif|jpg|png';
      // load library upload
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload('foto')) {
        $error = $this->upload->display_errors();
        // menampilkan pesan error
        print_r($error);
    } else {
      $result = $this->upload->data();
      $name = $result['file_name'];
      $this->personalia_model->insert_user($name);
      // get naon?
  	   redirect('personalia/users');
     }
    }
}
?>
