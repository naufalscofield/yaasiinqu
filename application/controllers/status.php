<?php
class status extends ci_controller{

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
    $this->load->model('status_model');
  }

  public function index($kode=null){

    if($kode = null){
      $this->load->view('components/header');
      $this->load->view('status/index');
      $this->load->view('components/footer');
    }else{

      $this->load->library('form_validation');

      $this->form_validation->set_rules('kode_order','Kode Order','required');

      if($this->form_validation->run() === FALSE){

        $this->load->view('components/header');
        $this->load->view('status/index');
        $this->load->view('components/footer');
      }else{
        $kode = $this->input->post('kode_order');

        $data['kode_row'] = $this->status_model->get_status_row($kode);
        $data['kode'] = $this->status_model->get_status($kode);
        if($data['kode_row'] > 0){

        $this->load->view('components/header');
        $this->load->view('status/index',$data);
        $this->load->view('components/footer');
      }else{
        echo"<script>alert('Kode Order yang anda cari tidak valid!'); window.location = 'status'</script>";
        }
      }
    }
  }

  public function upload_bukti(){
    $data['kode'] = $this->input->post('kode');

    $this->load->view('components/header');
    $this->load->view('status/upload',$data);
    $this->load->view('components/footer');
  }

  public function aksi_upload(){
    $kode = $this->input->post('kode');
    // print_r($kode); die;

    $config['upload_path'] = './assets/img/bukti';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '2000';
    // load library upload
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('bukti_transaksi')) {
        $error = $this->upload->display_errors();
        // menampilkan pesan error
        print_r($error);
    } else {
        $result = $this->upload->data();
        $bukti = $result['file_name'];
        $this->status_model->upload($bukti,$kode);
      echo"<script>alert('Bukti Transaksi Anda Sukses Di Upload,Terimakasih!'); window.location = '..'</script>";
    }
  }
}
?>
