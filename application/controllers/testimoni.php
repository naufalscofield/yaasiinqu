<?php
class testimoni extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
  }

  public function index(){
    $this->load->view('components/header');
    $this->load->view('testimoni/index');
    $this->load->view('components/footer');
  }
}

?>
