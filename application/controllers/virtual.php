<?php
class virtual extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->helper('url_helper');
    $this->load->model('insert_yasin');
  }

  public function index(){
    $data['yasin'] = $this->insert_yasin->get_yasin();

    $this->load->view('components/header');
    $this->load->view('virtual/index',$data);
    $this->load->view('components/footer');
  }
}
?>
