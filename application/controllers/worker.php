<?php
  class worker extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');

      if($this->session->userdata('status') != "login"){
      redirect(base_url("login"));
      }
      $this->load->helper('url_helper');
      $this->load->model('worker_model');
    }

    public function index(){
      $this->load->library('form_validation');

      $this->load->view('humans/worker/header');
      $this->load->view('humans/worker/sidebar');
      $this->load->view('humans/worker/profile');
      $this->load->view('humans/worker/footer');
    }

    public function update_profile(){
        $id_user = $this->input->post('id_user');
        $this->worker_model->update_profile($id_user);
        echo"<script>alert('Data profil berhasil di perbarui! Login kembali untuk validasi'); window.location = '../login/logout'</script>";
    }

    public function orders(){
      $data['orderan'] = $this->worker_model->get_order();

      $this->load->view('humans/worker/header');
      $this->load->view('humans/worker/sidebar');
      $this->load->view('humans/worker/orders',$data);
      $this->load->view('humans/worker/footer');

    }

    public function update_order($id_order){
      $this->worker_model->update_order($id_order,'done');
      redirect('worker/orders');
    }
  }
?>
