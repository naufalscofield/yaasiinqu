<?php
  class yasin extends ci_controller{

    public function __construct(){
      parent::__construct();
      $this->load->helper('url_helper');
      $this->load->model('insert_yasin');
    }

    public function index(){
      $this->load->view('yasin');
    }

    public function insert(){
      $this->insert_yasin->insert_data();
      redirect ('yasin');
    }

  }
?>
