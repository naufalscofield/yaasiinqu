<?php
class insert_model extends ci_model{

    public function __construct(){
    $this->load->database();
}

    public function insert_order($img_name){
    $this->load->helper('url');
    $tgl=date('Y-m-d');

    // $digitcantik = rand(111,999);
    // $nocantik = $this->input->post('no_telp');
    // // print_r($jadi); die;
    // $harga = $this->input->post('total_harga');
    // $ongkir = $this->input->post('ongkir');
    // // print_r($ongkir); die;
    // $jadi = "$digitcantik$nocantik";
    // $total = $harga+$ongkir+$digitcantik;


     // print_r($nocantik); die;

    $data = array(
      'kode_order' => $this->input->post('kode_order'),
      'pengorder' => $this->input->post('pengorder'),
      'tgl_order' => $tgl,
      'no_telp' => $this->input->post('no_telp'),
      'id_kota' => $this->input->post('kota'),
      'alamat' => $this->input->post('alamat'),
      'nama_alm' => $this->input->post('nama_alm'),
      'tempat_lahir' => $this->input->post('tempat_lahir'),
      'tanggal_lahir' => $this->input->post('tanggal_lahir'),
      'tempat_wafat' => $this->input->post('tempat_wafat'),
      'tanggal_wafat' => $this->input->post('tanggal_wafat'),
      'binti' => $this->input->post('binti'),
      'keluarga' => $this->input->post('keluarga'),
      'kode_desain' => $this->input->post('kode_desain'),
      'id_cover' => $this->input->post('id_cover'),
      'warna' => $this->input->post('warna'),
      'foto' => $img_name,
      'jumlah' => $this->input->post('jumlah'),
      'catatan' => $this->input->post('catatan'),
      'id_bank' => $this->input->post('payment'),
      'total' => $this->input->post('total'),
      'status' => 'waiting'
    );
    return $this->db->insert('tb_order',$data);
    }
}
?>
