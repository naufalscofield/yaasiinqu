<?php
  class admin_model extends ci_model{

    public function __construct(){
      parent::__construct();
      $this->load->database();
  }
  public function update_profile($id_user){
   $this->load->helper('url');

   $data = array(
     'nama_depan' => $this->input->post('nama_depan'),
     'nama_belakang' => $this->input->post('nama_belakang'),
     'username' => $this->input->post('username'),
     'password' => md5($this->input->post('password')),
     'alamat' => $this->input->post('alamat'),
     'no_hp' => $this->input->post('no_hp'),
     'email' => $this->input->post('email'),
     'level' => $this->input->post('level')
   );

   $this->db->where('id_user', $id_user);
   return $this->db->update('tb_user', $data);
 }

 public function get_user(){
     $query = $this->db->get('tb_user');
     return $query->result_array();
   }

   public function get_user_level($level){
     $this->db->select('*');
     $query = $this->db->get_where('tb_user',array('level' => $level));
     return $query->result_array();
   }

   public function delete_user($id_user){
      return $this->db->delete('tb_user',array('id_user'=>$id_user));
   }

   public function insert_user($img_name){
     $this->load->helper('url');


     $data = array(
    'nama_depan' => $this->input->post('nama_depan'),
    'nama_belakang' => $this->input->post('nama_belakang'),
    'username' => $this->input->post('username'),
    'password' => md5($this->input->post('password')),
    'alamat' => $this->input->post('alamat'),
    'no_hp' => $this->input->post('no_hp'),
    'email' => $this->input->post('email'),
    'level' => $this->input->post('level'),
    'foto' => $img_name
  );
  return $this->db->insert('tb_user',$data);
  }

    public function get_hasil(){
      $query = $this->db->get('tb_hasil');
      return $query->result_array();
    }

    public function delete_hasil($id_hasil){
      return $this->db->delete('tb_hasil',array('id_hasil'=>$id_hasil));
   }

    public function insert_hasil($name){
     $this->load->helper('url');


     $data = array(
    'foto' => $name
      );
      return $this->db->insert('tb_hasil',$data);
  }

///=================================================================////

    public function get_katalog(){
      $this->db->select('*');
      $this->db->from('tb_katalog');
      $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
      $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
      $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
      $query = $this->db->get();
      return $query->result_array();
}

public function get_katalog_where($id){
  $this->db->select('*');
  $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
  $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
  $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
  $query = $this->db->get_where('tb_katalog',array('tb_katalog.id_cover' => $id));
  return $query->result_array();
}

  public function get_katalog_id($id_desain){
    $query = $this->db->get_where('tb_katalog', array('id_desain' => $id_desain));
    return $query->row_array();
}

    public function get_cover() {
      $this->db->select('jenis_cover');
      $this->db->select('id_cover');
      $this->db->from('tb_cover');
      $query = $this->db->get();
      return $query->result_array();
}

    public function get_lembar() {
      $this->db->select('jumlah_lembar');
      $this->db->select('id_lembar');
      $this->db->from('tb_lembar');
      $query = $this->db->get();
      return $query->result_array();
}

    public function get_kertas() {
      $this->db->select('jenis_kertas');
      $this->db->select('id_kertas');
      $this->db->from('tb_kertas');
      $query = $this->db->get();
      return $query->result_array();
}

    public function delete_katalog($id_desain){
      return $this->db->delete('tb_katalog',array('id_desain'=>$id_desain));
}

    public function insert_katalog($name){
      $this->load->helper('url');


      $data = array(
        'kode_desain' => $this->input->post('kode_desain'),
        'foto' => $name,
        'id_cover' => $this->input->post('id_cover'),
        'id_lembar' => $this->input->post('id_lembar'),
        'id_kertas' => $this->input->post('id_kertas'),
        'warna' => $this->input->post('warna'),
        'harga' => $this->input->post('harga')
      );
        return $this->db->insert('tb_katalog',$data);
}

    public function update_katalog($id_desain,$data){
      $this->load->helper('url');

      $this->db->where('id_desain', $id_desain);
      return $this->db->update('tb_katalog', $data);
}

    public function update_status_katalog($id_desain,$data){
      $this->load->helper('url');

      $this->db->where('id_desain', $id_desain);
      return $this->db->update('tb_katalog', $data);
}

//=========================================================//
//=========================================================//

  public function get_payment() {
    $query = $this->db->get('tb_payment');
    return $query->result_array();
}

  public function get_payment_id($id_bank) {
    $query = $this->db->get_where('tb_payment',array('tb_payment','id_bank' => $id_bank));
    return $query->row_array();
}

  public function delete_payment($id_bank){
    return $this->db->delete('tb_payment',array('id_bank'=>$id_bank));
}

  public function insert_payment(){
    $this->load->helper('url');


    $data = array(
      'nama_bank' => $this->input->post('nama_bank'),
      'no_rek' => $this->input->post('no_rek'),
      'atas_nama' => $this->input->post('atas_nama'),
  );
      return $this->db->insert('tb_payment',$data);
}

//====================================================================//
    public function getcover(){
      $query = $this->db->get('tb_cover');
      return $query->result_array();
  }

    public function getkertas(){
      $query = $this->db->get('tb_kertas');
      return $query->result_array();
  }

    public function getlembar(){
      $query = $this->db->get('tb_lembar');
      return $query->result_array();
  }

    public function insert_cover(){
      $data = array(
        'jenis_cover' => $this->input->post('cover')
      );

      return $this->db->insert('tb_cover',$data);
    }

    public function insert_paper(){
      $data = array(
        'jenis_kertas' => $this->input->post('paper')
      );

      return $this->db->insert('tb_kertas',$data);
    }

    public function insert_pages(){
      $data = array(
        'jumlah_lembar' => $this->input->post('pages')
      );

      return $this->db->insert('tb_lembar',$data);
    }

    public function delete_cover($id_cover){
      return $this->db->delete('tb_cover',array('id_cover' => $id_cover));
    }

    public function delete_paper($id_kertas){
      return $this->db->delete('tb_kertas',array('id_kertas' => $id_kertas));
    }

    public function delete_pages($id_lembar){
      return $this->db->delete('tb_lembar',array('id_lembar' => $id_lembar));
    }

    public function get_kota(){
        $query = $this->db->get('tb_kota');
        return $query->result_array();
      }

    public function get_kota_id($id_kota){
        $query = $this->db->get_where('tb_kota',array('id_kota'=>$id_kota));
        // print_r($query); die;
        return $query->row_array();
      }

      public function insert_kota(){
        $data = array(
          'nama_kota' => $this->input->post('nama_kota'),
          'ongkir' => $this->input->post('ongkir')
        );

        return $this->db->insert('tb_kota',$data);
      }

      public function delete_kota($id_kota){
        return $this->db->delete('tb_kota',array('id_kota' => $id_kota));
      }
      public function update_kota(){
        $this->load->helper('url');
        $id_kota = $this->input->post('id_kota');

        $data = array(
          'nama_kota' => $this->input->post('nama_kota'),
          'ongkir' => $this->input->post('ongkir')
        );
        $this->db->where('id_kota',$id_kota);
        return $this->db->update('tb_kota',$data);

      }
}
 ?>
