<?php
  class ceo_model extends ci_model{

    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    public function update_profile($id_user){
      $this->load->helper('url');

      $data = array(
        'nama_depan' => $this->input->post('nama_depan'),
        'nama_belakang' => $this->input->post('nama_belakang'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'alamat' => $this->input->post('alamat'),
        'no_hp' => $this->input->post('no_hp'),
        'email' => $this->input->post('email'),
        'level' => $this->input->post('level')
      );

      $this->db->where('id_user', $id_user);
      return $this->db->update('tb_user', $data);
    }
    
    public function get_order2(){
      $query = $this->db->query("SELECT COUNT(a.id_cover) as jml, b.jenis_cover FROM tb_order a INNER JOIN tb_cover b ON a.id_cover=b.id_cover GROUP BY a.id_cover");
      return $query->result_array();
    }

    public function get_order(){
      $this->db->select('*');
      $this->db->from('tb_order');
      $this->db->join('tb_payment', 'tb_order.id_bank = tb_payment.id_bank');
      $this->db->join('tb_kota', 'tb_order.id_kota = tb_kota.id_kota');
      $query = $this->db->get();
      return $query->result_array();
    }

    public function get_order_id($status){
      $this->db->select('*');
      $this->db->join('tb_payment', 'tb_order.id_bank = tb_payment.id_bank');
      $this->db->join('tb_kota', 'tb_order.id_kota = tb_kota.id_kota');
      $query = $this->db->get_where('tb_order',array('status' => $status));
      return $query->result_array();
    }

    public function get_order_tanggal($tanggal){
      $this->db->select('*');
      $this->db->join('tb_payment', 'tb_order.id_bank = tb_payment.id_bank');
      $this->db->join('tb_kota', 'tb_order.id_kota = tb_kota.id_kota');
      $query = $this->db->get_where('tb_order',array('tgl_order' => $tanggal));
      return $query->result_array();
    }

    public function delete_order($id_order){
       return $this->db->delete('tb_order',array('id_order'=>$id_order));
    }

    public function get_user(){
      $query = $this->db->get('tb_user');
      return $query->result_array();
    }

    public function get_user_id($id_user){
      $query = $this->db->get_where('tb_user',array('id_user' => $id_user));
      return $query->row_array();
    }

    public function get_user_level($level){
      $this->db->select('*');
      $query = $this->db->get_where('tb_user',array('level' => $level));
      return $query->result_array();
    }

    public function delete_user($id_user){
       return $this->db->delete('tb_user',array('id_user'=>$id_user));
    }

    public function update_user(){
      $id = $this->input->post('id_user');
      $levelup = $this->input->post('levelup');

      $data = array('level' => $levelup);
      $this->db->where('id_user',$id);
      return $this->db->update('tb_user',$data);
    }

//================================================================//

  public function get_katalog(){
    $this->db->select('*');
    $this->db->from('tb_katalog');
    $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
    $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
    $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_katalog_where($id){
    $this->db->select('*');
    $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
    $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
    $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
    $query = $this->db->get_where('tb_katalog',array('tb_katalog.id_cover' => $id));
    return $query->result_array();
  }

  //================================================================//

    public function get_hasil(){
      $query = $this->db->get('tb_hasil');
      return $query->result_array();
    }

      //================================================================//

    public function get_cover(){
      $query = $this->db->get('tb_cover');
      return $query->result_array();
  }

    public function get_kertas(){
      $query = $this->db->get('tb_kertas');
      return $query->result_array();
  }

    public function get_lembar(){
      $query = $this->db->get('tb_lembar');
      return $query->result_array();
  }

      //================================================================//

    public function get_balance(){
      $query = $this->db->get('tb_payment');
      return $query->result_array();
  }

    public function get_balance_id($id_bank){
      $query = $this->db->get_where('tb_payment', array('id_bank' => $id_bank));
      return $query->row_array();
  }

    //================================================================//

      public function insert_balance(){
        $this->load->helper('url');


        $data = array(
          'nama_bank' => $this->input->post('nama_bank'),
          'no_rek' => $this->input->post('no_rek'),
          'atas_nama' => $this->input->post('atas_nama')
        );
        return $this->db->insert('tb_payment',$data);
   }

    public function update_balance($id_bank){
     $this->load->helper('url');

     $data = array(
       'nama_bank' => $this->input->post('nama_bank'),
       'no_rek' => $this->input->post('no_rek'),
       'atas_nama' => $this->input->post('atas_nama')
     );

     $this->db->where('id_bank', $id_bank);
     return $this->db->update('tb_payment', $data);
   }

   public function delete_balance($id_bank){
     return $this->db->delete('tb_payment',array('id_bank' => $id_bank));
   }

   //===================================================================//
   public function get_cover_sum(){
       $query = $this->db->query("SELECT id_cover,SUM(id_cover) AS cover FROM tb_order GROUP BY id_cover");

       if($query->num_rows() > 0){
           foreach($query->result() as $data){
               $hasil[] = $data;
           }
           return $hasil;
       }
   }

   public function get_kota(){
       $query = $this->db->get('tb_kota');
       return $query->result_array();
     }
}

?>
