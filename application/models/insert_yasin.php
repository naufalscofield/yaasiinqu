<?php
  class insert_yasin extends ci_model{

    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    public function insert_data(){
      $this->load->helper('url');

      $data = array(
        'terjemahan' => $this->input->post('terjemahan'),
        'terjemahan' => $this->input->post('terjemahan'),
        'latin' => $this->input->post('latin'),
        'arab' => $this->input->post('arab')
      );
      return $this->db->insert('tb_yasin',$data);
    }

    public function get_yasin(){
      $query = $this->db->get('tb_yasin');
      return $query->result_array();
    }
  }
?>
