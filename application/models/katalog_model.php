<?php
  class katalog_model extends ci_model{

    public function __construct(){
      $this->load->database();
    }

    public function get_katalog(){
      $this->db->select('*');
      $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
      $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
      $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
      $query = $this->db->get_where('tb_katalog',array('tb_katalog.status' => 'ready'));
      return $query->result_array();
    }

    public function get_katalog_where($id){
      $this->db->select('*');
      $this->db->join('tb_cover', 'tb_katalog.id_cover = tb_cover.id_cover');
      $this->db->join('tb_lembar', 'tb_lembar.id_lembar = tb_katalog.id_lembar');
      $this->db->join('tb_kertas', 'tb_kertas.id_kertas = tb_katalog.id_kertas');
      $query = $this->db->get_where('tb_katalog',array('tb_katalog.id_cover' => $id,'tb_katalog.status' => 'ready'));
      return $query->result_array();
    }

    public function get_color($id){
      $this->db->select('warna');
      $this->db->where('id_desain', $id);
      $query = $this->db->get('tb_katalog');
      $data = $query->result_array();
      // return $data;
      return $data[0]['warna'];
    }
  }
?>
