<?php
  class kota_model extends ci_model{

    public function __construct(){
      $this->load->database();
    }

    function get_kota() {
      $this->db->select('*');
      $this->db->from('tb_kota');
      $query = $this->db->get();
      return $query->result_array();
}
  }
?>
