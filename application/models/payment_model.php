<?php
  class payment_model extends ci_model{

    public function __construct(){
      $this->load->database();
    }

    function get_bank() {
      $this->db->select('*');
      $this->db->from('tb_payment');
      $query = $this->db->get();
      return $query->result_array();
}
  }
?>
