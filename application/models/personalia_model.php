<?php
  class personalia_model extends ci_model{

    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    public function update_profile($id_user){
      $this->load->helper('url');

      $data = array(
        'nama_depan' => $this->input->post('nama_depan'),
        'nama_belakang' => $this->input->post('nama_belakang'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'alamat' => $this->input->post('alamat'),
        'no_hp' => $this->input->post('no_hp'),
        'email' => $this->input->post('email'),
        'level' => $this->input->post('level')
      );

      $this->db->where('id_user', $id_user);
      return $this->db->update('tb_user', $data);
    }

    public function get_user(){
      $query = $this->db->get('tb_user');
      return $query->result_array();
    }

    public function get_user_level($level){
      $this->db->select('*');
      $query = $this->db->get_where('tb_user',array('level' => $level));
      return $query->result_array();
    }

    public function delete_user($id_user){
       return $this->db->delete('tb_user',array('id_user'=>$id_user));
    }

    public function insert_user($img_name){
      $this->load->helper('url');


      $data = array(
     'nama_depan' => $this->input->post('nama_depan'),
     'nama_belakang' => $this->input->post('nama_belakang'),
     'username' => $this->input->post('username'),
     'password' => md5($this->input->post('password')),
     'alamat' => $this->input->post('alamat'),
     'no_hp' => $this->input->post('no_hp'),
     'email' => $this->input->post('email'),
     'level' => $this->input->post('level'),
     'foto' => $img_name
   );
   return $this->db->insert('tb_user',$data);
   }


}
?>
