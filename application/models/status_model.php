<?php
  class status_model extends ci_model{

    public function __construct(){
      $this->load->database();
    }

    public function get_status_row($kode){
      $this->db->select('*');
      $this->db->where('kode_order', $kode);
      $query = $this->db->get('tb_order');
      $data = $query->num_rows();
      return $data;
    }

    public function get_status($kode){
      $this->db->select('*');
      $this->db->where('kode_order', $kode);
      $query = $this->db->get('tb_order');
      $data = $query->result_array();
      return $data;
    }

    public function upload($bukti,$kode){

      $data = array(
        'bukti_transaksi' => $bukti,
        'status' => 'paid'
      );
      // print_r($data); die;

      $this->db->where('kode_order',$kode);
      return $this->db->update('tb_order',$data);
    }
  }
?>
