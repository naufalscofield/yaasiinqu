<?php
  class worker_model extends ci_model{

    public function __construct(){
      parent::__construct();
      $this->load->database();
    }

    public function update_profile($id_user){
      $this->load->helper('url');

      $data = array(
        'nama_depan' => $this->input->post('nama_depan'),
        'nama_belakang' => $this->input->post('nama_belakang'),
        'username' => $this->input->post('username'),
        'password' => md5($this->input->post('password')),
        'alamat' => $this->input->post('alamat'),
        'no_hp' => $this->input->post('no_hp'),
        'email' => $this->input->post('email'),
        'level' => $this->input->post('level')
      );

      $this->db->where('id_user', $id_user);
      return $this->db->update('tb_user', $data);
    }

    public function get_order(){
      $query = $this->db->get_where('tb_order',array('status'=>'proccess'));
      return $query->result_array();
    }

    public function delete_order($id_order){
      return $this->db->delete('tb_order',array('id_order'=>$id_order));
    }

    public function update_order($id_order,$status){
      $data = array('status'=>$status);
      $this->db->where('id_order',$id_order);
      return $this->db->update('tb_order',$data);
    }
  }
?>
