<?php
  $hargabuku = $harga;
  $idcoverbuku = $id_cover;
?>
<section class="collective has-padding alternate-bg" id="order">
  <div class="container">

      <div class="col-md-7">
        <img src="<?php echo base_url() ."/assets/img/katalog/". $foto; ?>" style="height:200px;widht:200px"></center><br>

        <h4>Data Pengorder</h4>
        <form action="<?php echo base_url(); ?>order/insert" method="POST" enctype="multipart/form-data">
        <div class="form-group">
          <label>Pengorder</label>
          <input type="text" class="form-control" required name="pengorder" placeholder="Nama Pengorder">
        </div>

        <div class="form-group">
          <label>No Telepon</label>
          <input type="text" class="form-control" id="no_telp" required name="no_telp" placeholder="No Telepon" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
        </div>

        <div class="form-group">
          <label>Kota Tujuan</label><br>
          <select required name="kota" id="kota" class="form-control">
            <?php foreach ($kota as $kotas) { ?>
          <option value="<?php echo $kotas['id_kota'] ?>" data-ongkir="<?php echo $kotas['ongkir'] ?>"><?php echo $kotas['nama_kota'] ?></option>
        <?php } ?>
        </select>
        </div>


        <div class="form-group">
          <label>Alamat</label>
          <textarea type="text" class="form-control" required name="alamat" placeholder="Alamat Pengiriman" rows="8" cols="80"></textarea>
        </div><br>

        <!---------------------------------------------------------------------------------------->

        <br><h4>Data Almarhum/Almarhumah</h4>

        <div class="form-group">
          <label>Nama Almarhum/Almarhumah</label>
          <input type="text" class="form-control" required name="nama_alm" placeholder="Nama Almarhum/Almarhumah">
        </div>

        <div class="form-group">
            <label>Tempat Lahir</label>
            <input type="text" required class="form-control" required name="tempat_lahir">
        </div><br>

        <div class="form-group">
            <label>Tanggal Lahir</label>
            <input type="date" class="form-control" required id="tanggal_lahir" name="tanggal_lahir">
        </div><br>

        <div class="form-group">
          <label>Tempat Wafat</label>
          <input type="text" class="form-control" required name="tempat_wafat">
        </div><br>

        <div class="form-group">
            <label>Tanggal Wafat</label>
            <input type="date" class="form-control" required id="tanggal_wafat" name="tanggal_wafat">
        </div>

        <div class="form-group">
          <label>Binti</label>
          <input type="text" class="form-control" required name="binti" placeholder="Binti">
        </div>

        <div class="form-group">
        <label>Keluarga Yang Ditinggalkan</label>
        <textarea type="text" class="form-control" required name="keluarga" placeholder="Daftar Keluarga Yang Ditinggalkan : Contoh : anonim , anonim , anonim" rows="8" cols="80"></textarea>
      </div><br>

      <!---------------------------------------------------------------------------------------->

        <br><h4>Data Buku Yaasiin</h4>

        <div class="form-group">
          <label>Kode Desain</label>
          <input type="text" class="form-control" required name="kode_desain" readonly value="<?php echo $kode_desain; ?>" >
        </div>

          <input type="hidden" id="x_harga" required name="x_harga" value="<?php echo $harga; ?>">
          <input type="hidden" id="" required name="id_cover" value="<?php echo $id_cover; ?>">

        <div class="form-group">
          <label>Warna Dominan</label><br>
          <select required name="warna" id="inputArticle-Sort" class="form-control">
            <?php foreach($warna as $warnas) { ?>
              <option value="<?php echo $warnas; ?>"><?php echo $warnas; ?></option>
            <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label>Foto Almarhum/Almarhumah</label><br>
          <input type="file" required name="foto" value="" class="form-control">
        </div>

        <div class="form-group">
          <label>Jumlah</label><br>
          <center><button type="button" class="btn btn-default btn-sm krg_jml">-</button></center>
          <input type="number" id="jumlah" class="form-control jumlah" required name="jumlah" placeholder="Jumlah" value="60" min="12" readonly style="text-align:center">
          <center><button type="button" class="btn btn-default btn-sm tbh_jml">+</button></center>
        </div>

      <div class="form-group">
        <label>Catatan</label>
        <textarea type="text" class="form-control" name="catatan" placeholder="Tinggalkan Catatan Untuk Kami" ></textarea>
      </div>

      <div class="form-group">
        <label>Payment</label><br>
        <select required name="payment" id="payment" class="form-control">
          <?php foreach($payment as $payments) { ?>
            <option class="payment_child" data-bank="<?php echo $payments['nama_bank']; ?>" data-atasnama="<?php echo $payments['atas_nama']; ?>" data-norek="<?php echo $payments['no_rek']; ?>" value="<?php echo $payments['id_bank']; ?>"><?php echo $payments['nama_bank']; ?></option>
          <?php } ?>
        </select>
      </div><br>

        <input type="hidden" class="form-control" id="digitcantik" required name="digitcantik" readonly value="<?php echo rand(111,999); ?>" >

      <!---------------------------------------------------------------------------------------->

      <br><center><div class="form-group">
        <button type="button" id="btn_order" data-toggle="modal" data-target="#checkout" class="btn primary ">
          <i class="fa fa-paper-plane"></i>Order
        </button>
      </div>

      <div class="modal fade" id="checkout" role="dialog">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Checkout</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label>Total Harga Buku Yaasiin</label><br>
                    <label>Rp.</label><input type="text" style="text-align:center" class="form-control" id="total_harga" name="total_harga" placeholder="" readonly>
                </div>
                <div class="form-group">
                  <label>Ongkos Kirim</label>
                  <input type="text" style="text-align:center" class="form-control" id="ongkir_modal" name="ongkir_modal" value="" placeholder="" readonly>
                </div>
                <div class="form-group">
                  <label>Total Pembayaran </label>
                  <input type="text" style="text-align:center" class="form-control" id="total" name="total" value="" placeholder="" readonly>
                  <br><label>
                    <h5>TRANSFER PEMBAYARAN SAMPAI 3 DIGIT AKHIR</h5> ( 3 Digit akhir adalah kode unik anda )
                  </label><br>
                </div>
              <div class="form-group">
                <label>Pembayaran Ke Rekening Bank</label>
                <input type="text" style="text-align:center" class="form-control" id="bank" name="bank" value="" placeholder="" readonly>
              </div>
              <div class="form-group">
                <label>No Rekening</label>
                <input type="text" style="text-align:center" class="form-control" id="no_rek" name="bank" value="" placeholder="" readonly>
              </div>
              <div class="form-group">
                <label>Atas Nama</label>
                <input type="text" style="text-align:center" class="form-control" id="atas_nama" name="bank" value="" placeholder="" readonly>
              </div>
              <div class="form-group">
                <label>Kode Order Anda </label>
                <input type="text" style="text-align:center" class="form-control" id="kode_order" name="kode_order" value="" placeholder="" readonly>
              </div>
              <br><label>
                Gunakan Kode Order diatas untuk mengecek status pemesanan buku yaasiin anda<br>
                <br><center><button type="button" id="save" class="btn btn-primary" onclick="download('file text', 'myfilename.txt', 'text/plain')">Download Checkout Ini</button></center><br>
                <br><div class="modal-footer">
                  <button type="button" class="btn secondary pull-left" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn primary">Order</button>
                </div>
              </label><br>
              </div>
            </div>
          </div>
          </div>
        </div>

      </form>
      </div>
    </div>
</section>
<script async="" src="https://cdn.rawgit.com/eligrey/FileSaver.js/e9d941381475b5df8b7d7691013401e171014e89/FileSaver.min.js"></script>
<script type="text/javascript">

// Menjumlahkan Harga Buku Yaasiin + Jumlah Pcs
function cekLahir() {
  $('#tanggal_lahir').val() == ''
    ? $('#tanggal_wafat').attr("disabled", "true")
    : $('#tanggal_wafat').removeAttr("disabled", "true")
}
$(document).ready(function(){
  cekLahir()
  // checkJumlah()
  $('#btn_order').on('click', function(){
    var payment     = $('#payment').val();
    var id_bank     = $('#payment').data('id');
    var index = $("#payment").prop('selectedIndex') + 1
    var bank_name = $('#payment > option:nth-child('+index+')').attr('data-bank')
    var norek = $('#payment > option:nth-child('+index+')').attr('data-norek')
    var atasnama = $('#payment > option:nth-child('+index+')').attr('data-atasnama')
    var ongkir = $('#kota > option:nth-child('+index+')').attr('data-ongkir')

    $('#bank').val(bank_name)
    $('#no_rek').val(norek)
    $('#atas_nama').val(atasnama)
    $('#ongkir_modal').val(ongkir)

    var x_harga = $('#x_harga').val();
    var jumlah = $('#jumlah').val();
    var harga = x_harga * jumlah;
    $('#total_harga').val(harga)

    var harga   = $('#total_harga').val();
    var ongkir  = $('#ongkir_modal').val();
    var digit   = $('#digitcantik').val();
    var total   = parseInt(harga) + parseInt(ongkir) + parseInt(digit);
    $('#total').val(total)

    var no_telp     = $('#no_telp').val();
    var digit       = $('#digitcantik').val();
    var kode_order  = digit + no_telp;
    $('#kode_order').val(kode_order);

    console.log(aa)
  });

  $('#tanggal_lahir').on('change', function() {
    var tgl_lahir = $('#tanggal_lahir').val()
    $('#tanggal_wafat').attr('min', tgl_lahir)
    cekLahir()
  })
})

  function checkJumlah() {
    var jml = $('.jumlah').val();
    console.log(jml)
    if(jml < 12) {
      $('.krg_jml').attr('disabled', true);
    }else{
      $('.krg_jml').attr('disabled', false);
    }
  }

  function download(text, name, type) {
    var harga = $("#total_harga").val()
    var ongkir = $("#ongkir_modal").val()
    var total = $("#total").val()
    var bank = $("#bank").val()
    var norek = $("#no_rek").val()
    var atasnama = $("#atas_nama").val()
    var kode = $("#kode_order").val()
    var text = `Total Harga Buku Yaasiin Anda: Rp.${harga} // Ongkos Kirim Anda: Rp.${ongkir} // Total Pembayaran Anda: Rp.${total} (Transfer Sampai 3 Digit Terakhir)//
    Transfer Ke Rekening: ${bank} // No Rekening Bank: ${norek} // Rekening Bank Atas Nama: ${atasnama} // Kode Pembayaran Anda: ${kode} // Gunakan Kode Pembayaran Anda Untuk Mengecek Status Pemesanan Anda `
    var filename = 'Your Checkout - YaasiinQu'
    var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
    saveAs(blob, filename+".txt");
}

    // Menambahkan Jumlah Pcs Yaasiin
    $('.jumlah').blur(function() {
      var jml = $('.jumlah').val();
      if(jml == '') {
        $('.jumlah').val(0);
      }
      if(jml < 12) {
        $('.jumlah').val(12);
      }
    })

    $('.krg_jml').on('click',function() {
      var jml = $('.jumlah').val();

      if(jml > 12) {
        var next_jml = parseInt(jml)-1;
        $('.jumlah').val(next_jml);
      }
    })

    $('.tbh_jml').on('click',function() {
      var jml = $('.jumlah').val();
      var next_jml = parseInt(jml)+1;
      $('.jumlah').val(next_jml);
    })

    // Menambahkan Jumlah Pcs Yaasiin

    // Mengurangi Jumlah Pcs Yaasiin


</script>
