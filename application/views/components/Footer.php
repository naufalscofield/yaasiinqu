<!-- SECTION: Footer -->
<footer class="has-padding footer-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4 footer-branding">
        <img class="footer-branding-logo" src="<?php echo base_url() ?>assets/img/logo-putih.png" style="height:40px" alt="Synthetica freebie html5 css3 template logo">
        <ul class="footer-secondary-nav">
          <li><p>Situs order buku Yaasiin online pertama</a></p></li>
        </ul>
      </div>
    </div>
    <div class="row">
				<div class="col-md-12 footer-nav">
					<ul class="footer-primary-nav">
						<li><a href="<?php echo base_url(); ?>tentang">Tentang YaasiinQu</a></li>
						<li><a href="<?php echo base_url(); ?>panduan">Panduan Pemesanan</a></li>
					</ul>
					<ul class="footer-share">
						<li><p>Temukan kami di :</li>
						<li><a href="https://web.facebook.com/yaasiin.qu" ><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://instagram.com/yaasiinqu"><i class="fa fa-instagram"></i></a></li>
					</ul>
					<ul class="footer-secondary-nav">
						<li><p>© 2017 Hak Cipta PT YaasiinQu.com</p></li>
					</ul>
				</div>
			</div>
  </div>

</footer>
<!-- END SECTION: Footer -->
<!-- JS CDNs -->
<!-- jQuery local fallback -->
<script>
window.jQuery || document.write('<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/jquery-1.11.2.min.js"><\/script>')
</script>
<!-- JS Locals -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/retina.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/flickity.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/scripts-min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


    })
  </script>

<!-- <script src="<?php echo base_url(); ?>assets/bootstrap/js/min/jquery.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/min/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">
  $( document ).ready(function() {
      $('#datetimepicker1').datetimepicker();
  });
</script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID and uncomment -->
<!--
<script>
(function(b, o, i, l, e, r) {
  b.GoogleAnalyticsObject = l;
  b[l] || (b[l] =
    function() {
      (b[l].q = b[l].q || []).push(arguments)
    });
  b[l].l = +new Date;
  e = o.createElement(i);
  r = o.getElementsByTagName(i)[0];
  e.src = '//www.google-analytics.com/analytics.js';
  r.parentNode.insertBefore(e, r)
}(window, document, 'script', 'ga'));
ga('create', 'UA-XXXXX-X', 'auto');
ga('send', 'pageview');
</script>
-->
</body>

</html>
