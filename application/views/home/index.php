	<!--mulai header -->
	<header class="hero">
		<div class ="row">
		<div class="carousel koko">
			<div class="carousel-cell" style="background-image: url(<?php echo base_url() ?>assets/img/header/1.png);">
				<div class="hero-bg">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center">
								<h1 class="wp1">Pesan buku Yaasiin jadi lebih cepat dan lebih mudah di Yaasiin~Qu</h1>
								<a href="<?php echo base_url(); ?>katalog" class="btn primary wp2"><i class="fa fa-paper-plane-o"></i> Pesan Sekarang</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 hero-intro-text wp3">
								<p>Distributor buku Yaasiin terbesar di Indonesia &
									<span class="bold italic">order custom buku Yaasiin online petama di Indonesia</span>. </p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-cell" style="background-image: url(<?php echo base_url() ?>assets/img/header/2.png);">
				<div class="hero-bg">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center">
								<h1 class="wp1">YaasiinQu sebagai mitra resmi JNE</h1>
								<a href="<?php echo base_url(); ?>status" class="btn primary wp2"><i class="fa fa-check"> Cek Status Pesanan</i></a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 hero-intro-text wp3">
								<p>Pengiriman lebih cepat dan lebih aman.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-cell" style="background-image: url(<?php echo base_url() ?>assets/img/header/3.png);">
				<div class="hero-bg">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center">
								<h1 class="wp1">Kualitas terbaik dengan harga terbaik</h1>
								<a href="<?php echo base_url(); ?>katalog" class="btn primary wp2"><i class="fa fa-bookmark"> Lihat Katalog</i></a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 hero-intro-text wp3">
								<p>Cetakan rapi,desain terbaru.Garansi jika terdapat kesalahan.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='mouse-container'>
			<a href="#intro">
				<div class='mouse'>
					<span class='scroll-down'></span>
				</div>
			</a>
		</div>
	</div>
	</header>

	<!-- SECTION: Intro -->
	<!-- <section class="collective has-padding alternate-bg" id="intro">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h4>Apa itu YaasiinQu?</h4>
				</div>
				<div class="col-md-9">
					<p>Perusahaan percetakan buku Yaasiin terbesar dan pertama di Indonesia yang melayani pemesanan custom buku Yaasiin secara online.Pengerjaan cepat dengan lebih dari 100 pekerja profesional dan harga lebih murah.Hasil cetakan rapih dengan garansi cepat jika terdapat kesalahan.</p>
				</div>
			</div>
				</div>
	</section> -->
	<!-- END SECTION: Intro -->

	<!-- SECTION: Promo -->
	<section class="collective has-padding" id="intro">
		<div class="container">

				<div class="col-md-12 wp4">
				<div class="col-md-4">
					<img src="<?php echo base_url() ?>assets/img/wanita.png" alt="wanita-muslimah" style="height:300px">
				</div>
				<div class="col-md-8">
					<p style="display:inline">Pelayanan terbaik untuk Yaasiin terbaik,lebih dari 1000 konsumen sudah pesan buku Yaasiin di YaasiinQu.
					Pengerjaan lebih cepat dengan lebih dari 100 pekerja.Harga lebih murah dengan status distributor.Pengiriman lebih cepat dengan
					JNE menjadi mitra resmi YaasiinQu.Garansi cepat jika ada kesalahan cetak.</p>
				</div><br>
				<br><div class="col-md-8">
					<a href="http://instagram.com/yaasiinqutestimonial" class="btn primary wp2"><i class="fa fa-star"> Lihat Testimonial</i></a>
				</div>
				<div class="col-md-10">
				</div>
			</div>
		</div>
	</section>
	<!-- END SECTION: Promo -->

	<!-- SECTION: Crew -->
	<section class="crew has-padding alternate-bg" id="team">
		<div class="container">
				<div class="col-md-12">
					<h4>Hasil Kerja Kami</h4>
				</div>
					<?php foreach ($hasil as $hasils) { ?>
					<article class=""><img src="<?php echo base_url() ."/assets/img/hasil/". $hasils['foto']; ?>" style="width:250px;height:250px"></article><br>
				<?php } ?>
			<br>
				<br><center><a href="<?php echo base_url(); ?>katalog" class="btn primary wp2"><i class="fa fa-bookmark"> Lihat Katalog</i></a></center>
			</div>
	</section>
	<!-- END SECTION: Crew -->
	<!-- SECTION: Get started -->
	<section class="get-started has-padding text-center" id="get-started">
		<div class="container">
			<div class="row">
				<div class="col-md-12 wp4">
					<h2>Cek Ongkos Kirim Ke Kota Mu!</h2><br>
					<br><a href="http://www.jne.co.id/en/tracking/tarif" class="btn secondary-white">Cek Disini</a>
				</div>
			</div>
		</div>
	</section>
</div>

	<!-- END SECTION: Get started -->
<script type="text/javascript">
	$(document).ready(function() {
		$('.koko').flickity({
		  // options
		  // cellAlign: 'left',
			autoPlay: 5000,
		  contain: true
		});

		var flkty = Flickity.data( $('.koko')[0] )
		var $carousel = $('.koko').flickity()

		$(".koko").on( 'settle', function() {
	    if(flkty.selectedIndex === 2){
       console.log("last!")
       // $carousel.flickity('stopPlayer');
	    }
		})
	})
</script>
