<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Examples
                    <a id="add_hasil" href="#form_insert" class="btn btn-sm btn-success"><i class="fa fa-plus fa-fw" aria-hidden="true"></i><i class="fa fa-book fa-fw" aria-hidden="true"></i>New Example</a></div>
                  </h4>
             </div>

        <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><center></th>
                                    <th><center><b>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($hasil as $hasils) { ?>
                                <tr>
                                    <td><center><img src="<?php echo base_url() ."/assets/img/hasil/". $hasils['foto']; ?>" style="width:100px; height:100px">
                                    </td>
                                    <td><center>
                                    <a href="<?php echo site_url('admin/delete_hasil/'.$hasils['id_hasil']); ?>"class="btn btn-sm btn-danger">Delete</a>
                                    </td><?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>


      <section id="form_insert" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_hasil" method="POST" enctype="multipart/form-data">
                      <div class="form-group">
                          <label class="col-md-12">Foto Hasil</label>
                          <div class="col-md-12">
                              <input type="file" name="foto" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Post New Example</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>

    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_hasil').on('click', function() {
      $('#form_insert').attr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
