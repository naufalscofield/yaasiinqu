<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Katalog
                    <a id="add_katalog" class="btn btn-sm btn-success" href="#form_insert"><i class="fa fa-plus fa-fw" aria-hidden="true"></i><i class="fa fa-bookmark fa-fw" aria-hidden="true"></i>New Catalouge</a></div>
                  </h4>
                  <ol class="breadcrumb">
                     <li class="active">Filter Jenis Cover</li>
                     <form action = "<?php echo base_url(); ?>admin/katalog" method="POST">
                     <select name="id" id="inputArticle-Sort" class="form-control form-control-line">
                      <option value="">All Cover</option>
                      <option value="1">Soft Cover</option>
                      <option value="2">Hard Cover</option>
                      <option value="3">Blurdru Cover</option>
                    </select>
                      <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                        <i class="fa fa-filter"></i> Filter
                      </button></center>
                  </form>
                 </ol>
             </div>


      <?php if (empty($data_katalog)){ ?>
        <section id="data-katalog" >
          <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr>
                                    <th><center><b>Design Code</th>
                                    <th><center><b>Picture</th>
                                    <th><center><b>Cover</th>
                                    <th><center><b>Pages</th>
                                    <th><center><b>Paper</th>
                                    <th><center><b>Color</th>
                                    <th><center><b>Price</th>
                                    <th><center><b>Status</th>
                                    <th width="300"><center><b>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($katalog as $katalogs) { ?>
                                <tr>
                                    <td><center><?php echo $katalogs['kode_desain']; ?></td>
                                    <td><center><img src="<?php echo base_url() ."/assets/img/katalog/". $katalogs['foto']; ?>" style="width:100px; height:100px"></td>
                                    <td><center><?php echo $katalogs['jenis_cover']; ?></td>
                                    <td><center><?php echo $katalogs['jumlah_lembar']; ?></td>
                                    <td><center><?php echo $katalogs['jenis_kertas']; ?></td>
                                    <td><center><?php echo $katalogs['warna']; ?></td>
                                    <td><center><?php echo $katalogs['harga']; ?></td>
                                    <td><center><?php echo $katalogs['status']; ?></td>
                                    <td><center>
                                    <a href="<?php echo site_url('admin/delete_katalog/'.$katalogs['id_desain']); ?>"class="btn btn-sm btn-danger">Delete</a>
                                    <a href="<?php echo site_url('admin/katalog/'.$katalogs['id_desain']); ?>"class="btn btn-sm btn-info">Edit</a>
                                    <form action="<?php echo base_url(); ?>admin/update_status_katalog" method="post">
                                      <select name="status" class="form-control-line">
                                        <option value="ready">Ready</option>
                                        <option value="oos">Out Of Stock</option>
                                     </select>
                                   <input type="hidden" name="id_desain" value="<?php echo $katalogs['id_desain']; ?>">
                                   <button type="submit" name="button" class="btn btn-sm btn-warning">Update Status</button>
                                 </form>
                                    </td><?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>
    <?php } ?>

      <section id="form_insert" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_katalog" method="POST" enctype="multipart/form-data">
                      <div class="form-group">
                          <label class="col-md-12">Design Code</label>
                          <div class="col-md-12">
                              <input type="text" required name="kode_desain" placeholder="" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Photo</label>
                          <div class="col-md-12">
                              <input type="file" required name="foto" placeholder="" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                        <label>Cover</label><br>
                          <select required name="id_cover" id="inputArticle-Sort" class="form-control">
                            <?php foreach($cover as $covers) { ?>
                              <option value="<?php echo $covers['id_cover']; ?>"><?php echo $covers['jenis_cover']; ?></option>
                            <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Pages</label><br>
                          <select required name="id_lembar" id="inputArticle-Sort" class="form-control">
                            <?php foreach($lembar as $lembars) { ?>
                              <option value="<?php echo $lembars['id_lembar']; ?>"><?php echo $lembars['jumlah_lembar']; ?></option>
                            <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Paper</label><br>
                          <select required name="id_kertas" id="inputArticle-Sort" class="form-control">
                            <?php foreach($kertas as $kertass) { ?>
                              <option value="<?php echo $kertass['id_kertas']; ?>"><?php echo $kertass['jenis_kertas']; ?></option>
                            <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Color</label>
                          <div class="col-md-12">
                              <input type="text" required name="warna" placeholder="warna1,warna2,warna3" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Price</label>
                          <div class="col-md-12">
                              <input type="text" required name="harga" placeholder="" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Post New Katalog</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>

      <?php if (!empty($data_katalog)){ ?>
           <section id="katalog">
               <div class="col-md-8 col-xs-12">
                   <div class="white-box">
                     <form class="form-horizontal form-material" action="<?php echo base_url();?>admin/update_katalog" method="post" enctype="multipart/form-data">
                     <div class="form-group">
                         <label class="col-md-12">Design Code</label>
                         <div class="col-md-12">
                             <input type="text" required name="kode_desain" placeholder="" value="<?php echo $data_katalog['kode_desain']; ?>" class="form-control form-control-line" >
                             <input type="hidden" required name="id_desain" placeholder="" value="<?php echo $data_katalog['id_desain']; ?>" class="form-control form-control-line" >
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-md-12">Photo</label>
                         <div class="col-md-12">
                             <input type="file" name="foto" placeholder="" value="<?php echo base_url() ."assets/img/katalog/". $data_katalog['foto']; ?>" class="form-control form-control-line" >
                         </div>
                     </div>
                     <div class="form-group">
                       <label>Cover</label><br>
                         <select required name="id_cover" id="inputArticle-Sort" class="form-control">
                           <?php foreach($cover as $covers) { ?>
                             <option value="<?php echo $covers['id_cover']; ?>"><?php echo $covers['jenis_cover']; ?></option>
                           <?php } ?>
                         </select>
                     </div>
                     <div class="form-group">
                       <label>Pages</label><br>
                         <select required name="id_lembar" id="inputArticle-Sort" class="form-control">
                           <?php foreach($lembar as $lembars) { ?>
                             <option value="<?php echo $lembars['id_lembar']; ?>"><?php echo $lembars['jumlah_lembar']; ?></option>
                           <?php } ?>
                         </select>
                     </div>
                     <div class="form-group">
                       <label>Paper</label><br>
                         <select required name="id_kertas" id="inputArticle-Sort" class="form-control">
                           <?php foreach($kertas as $kertass) { ?>
                             <option value="<?php echo $kertass['id_kertas']; ?>"><?php echo $kertass['jenis_kertas']; ?></option>
                           <?php } ?>
                         </select>
                     </div>
                     <div class="form-group">
                         <label class="col-md-12">Color</label>
                         <div class="col-md-12">
                             <input type="text" required name="warna" value="<?php echo $data_katalog['warna']; ?>" placeholder="warna1,warna2,warna3" class="form-control form-control-line" >
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-md-12">Price</label>
                         <div class="col-md-12">
                             <input type="text" required name="harga" value="<?php echo $data_katalog['harga']; ?>" placeholder="" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                         </div>
                     </div>
                           <div class="form-group">
                               <div class="col-sm-12">
                                   <button type ="submit" class="btn btn-warning">Update Katalog</button>
                                 </form>
                               </div>
                           </div>
                       </form>
                   </div>
               </div>
           </section>
           <?php } ?>

    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_katalog').on('click', function() {
      $('#form_insert').attr('style', "display: 'block'");
      // $('#data-katalog').attr('style', "display: 'none'");
      // $('#table').removeAttr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
