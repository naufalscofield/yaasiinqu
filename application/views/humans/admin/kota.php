<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Cities <br>
                    <a id="add_city" class="btn btn-sm btn-success" href="#form_insert"><i class="fa fa-plus fa-fw" aria-hidden="true"></i><i class="fa fa-bookmark fa-fw" aria-hidden="true"></i>New City</a></div>
                  </h4
             </div>


      <?php if (empty($data_kota)){ ?>
        <section id="data-kota" >
          <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table" id="table">
                            <thead>
                                <tr>
                                    <th><center><b>City</th>
                                    <th><center><b>Cost</th>
                                    <th width="300"><center><b>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($kota as $kotas) { ?>
                                <tr>
                                    <td><center><?php echo $kotas['nama_kota']; ?></td>
                                    <td><center><?php echo $kotas['ongkir']; ?></td>
                                    <td><center>
                                    <a href="<?php echo site_url('admin/delete_city/'.$kotas['id_kota']); ?>"class="btn btn-sm btn-danger">Delete</a>
                                    <a href="<?php echo site_url('admin/city/'.$kotas['id_kota']); ?>"class="btn btn-sm btn-info">Edit</a>
                                    </td><?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>
    <?php } ?>

      <section id="form_insert" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_city" method="POST" enctype="multipart/form-data">
                      <div class="form-group">
                          <label class="col-md-12">City</label>
                          <div class="col-md-12">
                              <input type="text" required name="nama_kota" placeholder="" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Cost</label>
                          <div class="col-md-12">
                              <input type="text" required name="ongkir" placeholder="" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Post New City</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>

      <?php if (!empty($data_kota)){ ?>
           <section id="city">
               <div class="col-md-8 col-xs-12">
                   <div class="white-box">
                     <form class="form-horizontal form-material" action="<?php echo base_url();?>admin/update_city" method="post" enctype="multipart/form-data">
                     <div class="form-group">
                         <label class="col-md-12">City</label>
                         <div class="col-md-12">
                              <input type="text" required name="nama_kota" placeholder="" value="<?php echo $data_kota['nama_kota']; ?>" class="form-control form-control-line" >
                              <input type="hidden" required name="id_kota" placeholder="" value="<?php echo $data_kota['id_kota']; ?>" class="form-control form-control-line" >
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-md-12">Cost</label>
                         <div class="col-md-12">
                             <input type="text" required name="ongkir" value="<?php echo $data_kota['ongkir']; ?>" placeholder="" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                         </div>
                     </div>
                           <div class="form-group">
                               <div class="col-sm-12">
                                   <button type ="submit" class="btn btn-warning">Update Katalog</button>
                                 </form>
                               </div>
                           </div>
                       </form>
                   </div>
               </div>
           </section>
           <?php } ?>

    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_city').on('click', function() {
      $('#form_insert').attr('style', "display: 'block'");
      // $('#data-city').attr('style', "display: 'none'");
      // $('#table').removeAttr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
