<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Materials
                  </h4>
             </div>

        <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="100"><center><a href="#insert_cover" id="add_cover" class="btn btn-sm btn-success"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>New Cover</a></center></th>
                                    <th width="100"><center><b>Covers</center></th>
                                    <th width="100"><center><b>Action</center></th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($cover as $covers) { ?>
                                <tr>
                                    <td><center></center></td>
                                    <td><center><?php echo $covers['jenis_cover']; ?></center></td>
                                    <td><center><a href="<?php echo site_url('admin/delete_cover/'.$covers['id_cover']); ?>"class="btn btn-sm btn-danger">Delete</a></center></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table><br>

                    <br><div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="100"><center><a href="#insert_paper" id="add_paper" class="btn btn-sm btn-success"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>New Paper</a></center></th>
                                    <th width="100"><center><b>Papers</center></th>
                                    <th width="100"><center><b>Action</center></th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($kertas as $kertass) { ?>
                                <tr>
                                    <td><center></center></td>
                                    <td><center><?php echo $kertass['jenis_kertas']; ?></center></td>
                                    <td><center><a href="<?php echo site_url('admin/delete_paper/'.$kertass['id_kertas']); ?>"class="btn btn-sm btn-danger">Delete</a></center></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table><br>

                    <br><div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="100"><center><a href="#insert_pages" id="add_pages" class="btn btn-sm btn-success"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>New Pages</a></center></th>
                                    <th width="100"><center><b>Pages</center></th>
                                    <th width="100"><center><b>Action</center></th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($lembar as $lembars) { ?>
                                <tr>
                                    <td><center></center></td>
                                    <td><center><?php echo $lembars['jumlah_lembar']; ?></center></td>
                                    <td><center><a href="<?php echo site_url('admin/delete_pages/'.$lembars['id_lembar']); ?>"class="btn btn-sm btn-danger">Delete</a></center></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>

      <?php echo validation_errors(); ?>
      <section id="insert_cover" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_cover" method="POST">
                      <div class="form-group">
                          <label class="col-md-12">Cover</label>
                          <div class="col-md-12">
                              <input type="text" required name="cover" placeholder="cover" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Create New Cover</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>

      <?php echo validation_errors(); ?>
      <section id="insert_paper" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_paper" method="POST">
                      <div class="form-group">
                          <label class="col-md-12">Paper</label>
                          <div class="col-md-12">
                              <input type="text" name="paper" required placeholder="paper" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Create New Paper</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>

      <?php echo validation_errors(); ?>
      <section id="insert_pages" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form class="form-horizontal form-material" action="<?php echo base_url(); ?>admin/insert_pages" method="POST">
                      <div class="form-group">
                          <label class="col-md-12">Pages</label>
                          <div class="col-md-12">
                              <input type="text" name="pages" required placeholder="pages" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Create New Pages</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>


    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_cover').on('click', function() {
      $('#insert_cover').attr('style', "display: 'block'");
    })

    $('#add_paper').on('click', function() {
      $('#insert_paper').attr('style', "display: 'block'");
    })

    $('#add_pages').on('click', function() {
      $('#insert_pages').attr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
