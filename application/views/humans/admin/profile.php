<?php
$photo_name = $this->session->userdata('foto');
// print_r($photo_name); die; ?>
 <div id="page-wrapper">
     <div class="container-fluid">
         <div class="row bg-title">
             <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                 <h4 class="page-title">Assalamualaikum <?php echo $this->session->userdata('nama_depan') ?></h4> </div>

         </div>
         <!-- /.row -->
         <!-- .row -->
         <div class="row">
             <div class="col-md-4 col-xs-12">
                 <div class="white-box">
                     <center><div class="user"><img src="<?php echo base_url() ."/assets/img/users/". $photo_name; ?>" style="height:300px;widht:300px"><center>
                     </div>
                 </div>
             </div>

             <div class="col-md-8 col-xs-12">
                 <div class="white-box">
                     <form class="form-horizontal form-material" action="<?= base_url(); ?>admin/update_profile" method="post">
                       <input name="id_user" value="<?php echo $this->session->userdata('id_user') ?>" type="hidden" placeholder="Johnathan" class="form-control form-control-line"> </div>
                         <div class="form-group">
                             <label class="col-md-12">First Name</label>
                             <div class="col-md-12">
                                 <input name="nama_depan" required value="<?php echo $this->session->userdata('nama_depan') ?>" type="text" placeholder="Johnathan" class="form-control form-control-line"> </div>
                         </div>
                         <div class="form-group">
                             <label class="col-md-12">Last Name</label>
                             <div class="col-md-12">
                                 <input type="text" required value="<?php echo $this->session->userdata('nama_belakang') ?>" name="nama_belakang" placeholder="Doe" class="form-control form-control-line"> </div>
                         </div>
                         <div class="form-group">
                             <label class="col-md-12">Username</label>
                             <div class="col-md-12">
                                 <input type="text" required value="<?php echo $this->session->userdata('username') ?>" name="username" placeholder="johndoe" class="form-control form-control-line"> </div>
                         </div>
                         <div class="form-group">
                           <label class="col-md-12">Password</label>
                           <div class="col-md-12">
                             <input type="password" required value="<?php echo $this->session->userdata('pw') ?>" name="password" value="password" class="form-control form-control-line"> </div>
                           </div>
                           <div class="form-group">
                             <label class="col-md-12">Address</label>
                             <div class="col-md-12">
                               <textarea rows="5" required name="alamat" class="form-control form-control-line"><?php echo $this->session->userdata('alamat') ?></textarea>
                             </div>
                           </div>
                           <div class="form-group">
                             <label class="col-md-12">Phone No</label>
                             <div class="col-md-12">
                               <input type="text" required name="no_hp" value="<?php echo $this->session->userdata('no_hp') ?>" placeholder="123 456 7890" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'> </div>
                             </div>
                         <div class="form-group">
                             <label for="example-email" class="col-md-12">Email</label>
                             <div class="col-md-12">
                                 <input type="email" required value="<?php echo $this->session->userdata('email') ?>" name="email" placeholder="johnathan@Yaasinqu.com" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                         </div>
                         <input name="level" required value="<?php echo $this->session->userdata('level') ?>" type="hidden" placeholder="Johnathan" class="form-control form-control-line">
                         <div class="form-group">
                             <div class="col-md-12"><br>
                                 <button class="btn btn-success" type="submit">Update Profile</button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
         <!-- /.row -->
     </div>
