<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Account Balances
                    <!-- <button id="add_balance" class="btn btn-sm btn-success"><i class="fa fa-plus fa-fw" aria-hidden="true"></i><i class="fa fa-cc-visa fa-fw" aria-hidden="true"></i>New Account</button></div> -->
                  </h4>
             </div>

        <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><center><b>Bank</th>
                                    <th><center><b>Balance Number</th>
                                    <th><center><b>Name Of</th>
                                    <!-- <th><center><b>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($balance as $balances) { ?>
                                <tr>
                                    <td><center><?php echo $balances['nama_bank']; ?></td>
                                    <td><center><?php echo $balances['no_rek']; ?></td>
                                    <td><center><?php echo $balances['atas_nama']; ?></td>
                                    <!-- <td><center>
                                    <a href="<?php echo site_url('ceo/delete_balance/'.$balances['id_bank']); ?>"class="btn btn-sm btn-danger">Delete</a>
                                    <a href="<?php echo site_url('ceo/balances/'.$balances['id_bank']); ?>"class="btn btn-sm btn-info">Change Name Of Balance</a>
                                    </td> -->
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                      </section>

        <section id="form_insert" style="display: none">
           <div class="col-md-8 col-xs-12">
               <div class="white-box">
                   <form class="form-horizontal form-material" action="<?php echo base_url(); ?>ceo/insert_balance" method="POST">
                       <div class="form-group">
                           <label class="col-md-12">Bank</label>
                           <div class="col-md-12">
                               <input required type="text" name="nama_bank" placeholder="" class="form-control form-control-line" >
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-12">Account Number</label>
                           <div class="col-md-12">
                               <input required type="text" name="no_rek" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control form-control-line">
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-12">Name Of</label>
                           <div class="col-md-12">
                               <input required type="text" name="atas_nama" placeholder="" class="form-control form-control-line">
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-sm-12">
                               <button type ="submit" class="btn btn-success">Create New Balance</button>
                           </div>
                       </div>
                   </form>
               </div>
           </div>
       </section>

<?php if (!empty($data_balance)){ ?>
<section id="update_account">
    <div class="col-md-8 col-xs-12">
        <div class="white-box">
          <?php echo form_open('ceo/update_balance/'.$data_balance['id_bank']); ?>
                <div class="form-group">
                    <label class="col-md-12">Bank</label>
                    <div class="col-md-12">
                        <input type="text" readonly name="nama_bank" value="<?php echo $data_balance['nama_bank'] ?>" placeholder="" class="form-control form-control-line" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Account Number</label>
                    <div class="col-md-12">
                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' readonly name="no_rek" value="<?php echo $data_balance['no_rek'] ?>" placeholder="" class="form-control form-control-line" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Name Of</label>
                    <div class="col-md-12">
                        <input type="text" required name="atas_nama" value="<?php echo $data_balance['atas_nama'] ?>" placeholder="" class="form-control form-control-line" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type ="submit" class="btn btn-warning">Change The Name Of Balance</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<?php } ?>

    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_balance').on('click', function() {
      $('#form_insert').attr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
