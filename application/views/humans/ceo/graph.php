<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div style="margin-left:220px; background-color: #fff">
  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>

  <table id="datatable">
      <thead>
          <tr>
              <th>Grafik Penjualan</th>
        <?php
          foreach ($orderan as $orderans) {
        ?>
              <td><?php echo $orderans['jenis_cover'].' - '.$orderans['jml'] ?></td>
        <?php
          }
        ?>
          </tr>
      </thead>
      <tbody>
        <?php
          foreach ($orderan as $orderans) {
        ?>
          <tr>
              <th><?php echo $orderans['jenis_cover'] ?></th>
              <td><?php echo $orderans['jml'] ?></td>
          </tr>
        <?php
          }
        ?>
      </tbody>
    </table>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    Highcharts.chart('container', {
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Penjualan Berdasarkan Jenis Cover'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Units'
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' ' + this.point.name.toLowerCase();
            }
        }
    });
  })
</script>
