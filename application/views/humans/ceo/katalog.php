<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Catalogues
                  </h4>
                </div>
                  <ol class="breadcrumb">
                     <li class="active">Filter Jenis Cover</li>
                     <form action = "<?php echo base_url(); ?>ceo/catalogues" method="POST">
                     <select name="id" id="inputArticle-Sort" class="form-control form-control-line">
                      <option value="">All Katalog</option>
                      <option value="1">Soft Cover</option>
                      <option value="2">Hard Cover</option>
                      <option value="3">Blurdru Cover</option>
                    </select>
                      <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                        <i class="fa fa-filter"></i> Filter
                      </button></center>
                  </form>
                 </ol>

        <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><center><b>Design Code</th>
                                    <th><center><b>Picture</th>
                                    <th><center><b>Cover</th>
                                    <th><center><b>Pages</th>
                                    <th><center><b>Paper</th>
                                    <th><center><b>Color</th>
                                    <th><center><b>Price</th>
                                    <th><center><b>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($katalog as $katalogs) { ?>
                                <tr>
                                    <td><center><?php echo $katalogs['kode_desain']; ?></td>
                                    <td><center><img src="<?php echo base_url() ."/assets/img/katalog/". $katalogs['foto']; ?>" style="width:100px; height:100px">
                                    </td>
                                    <td><center><?php echo $katalogs['jenis_cover']; ?></td>
                                    <td><center><?php echo $katalogs['jumlah_lembar']; ?></td>
                                    <td><center><?php echo $katalogs['jenis_kertas']; ?></td>
                                    <td><center><?php echo $katalogs['warna']; ?></td>
                                    <td><center><?php echo $katalogs['harga']; ?></td>
                                    <td><center><?php echo $katalogs['status']; ?></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>

<footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>



</body>

</html>
