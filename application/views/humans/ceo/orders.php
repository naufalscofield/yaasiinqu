        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">The Orders</h4>
                      </div>

                        <ol class="breadcrumb">
                           <li class="active">Search By Date</li>
                           <form class="" action="<?php echo base_url(); ?>ceo/orders_date" method="post">
                           <input type="date" name="tanggal" class="form-control form-control">
                          <center><button type="submit" id="btn_order" class="btn btn-sm btn-danger">
                            <i class="fa fa-search"></i> Search
                          </button>
                          </form>
                        </ol>

                           <!-- <li class="active">Search By Month</li>
                           <form class="" action="<?php echo base_url(); ?>ceo/orders_date" method="post">
                             <select name="status" id="inputArticle-Sort" class="form-control form-control-line">
                              <option value=""></option>
                              <option value="1">Januari</option>
                              <option value="2">Februari</option>
                              <option value="3">Maret</option>
                            </select>
                          <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                            <i class="fa fa-search"></i> Search
                          </button>
                          </form>

                           <li class="active">Search By Year</li>
                           <form class="" action="<?php echo base_url(); ?>ceo/orders_date" method="post">
                           <input type="date" name="tanggal" class="form-control form-control-line">
                          <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                            <i class="fa fa-search"></i> Search
                          </button>
                          </form> -->
                          &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                          <ol class="breadcrumb">
                           <li class="active">Filter Status</li>
                           <form class="" action="<?php echo base_url(); ?>ceo/orders" method="post">
                           <select name="status" id="inputArticle-Sort" class="form-control form-control">
                 						<option value="">All Status</option>
                 						<option value="waiting">Waiting</option>
                 						<option value="paid">Paid</option>
                 						<option value="proccess">Proccess</option>
                 						<option value="done">Done</option>
                 					</select>
                          <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                            <i class="fa fa-filter"></i> Filter
                          </button>
                          </form>
                       </ol>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                                <table class="table">
                                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                  </div>
                                    <thead>
                                        <tr>
                                            <th><center><b>Order Date</th>
                                            <th><center><b>Order Code</th>
                                            <th><center><b>Buyer</th>
                                            <th><center><b>Phone Number</th>
                                            <th><center><b>City</th>
                                            <th><center><b>Design Code</th>
                                            <th><center><b>Color</th>
                                            <th><center><b>Amount</th>
                                            <th><center><b>Payment Bank</th>
                                            <th><center><b>Total</th>
                                            <th><center><b>Status</th>
                                            <th><center><b>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($orderan as $orderans) { ?>
                                        <tr>
                                            <td><center><?php echo $orderans['tgl_order']; ?></td>
                                            <td><center><?php echo $orderans['kode_order']; ?></td>
                                            <td><center><?php echo $orderans['pengorder']; ?></td>
                                            <td><center><?php echo $orderans['no_telp']; ?></td>
                                            <td><center><?php echo $orderans['nama_kota']; ?></td>
                                            <td><center><?php echo $orderans['kode_desain']; ?></td>
                                            <td><center><?php echo $orderans['warna']; ?></td>
                                            <td><center><?php echo $orderans['jumlah']; ?></td>
                                            <td><center><?php echo $orderans['nama_bank']; ?></td>
                                            <td><center><?php echo $orderans['total']; ?></td>
                                            <td><center><?php echo $orderans['status']; ?></td>
                                            <td><center><a href="<?php echo site_url('ceo/delete_order/'.$orderans['id_order']); ?>"class="btn btn-sm btn-danger">Delete</a></td><?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>
</body>

</html>
