<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
   <div class="sidebar-nav navbar-collapse slimscrollsidebar">
       <ul class="nav" id="side-menu">
            <li style="padding: 10px 0 0;">
               <a href="<?php echo base_url(); ?>ceo" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i><span class="hide-menu">Profile</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/orders" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i><span class="hide-menu">Orders</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/users" class="waves-effect"><i class="fa fa-users fa-fw" aria-hidden="true"></i><span class="hide-menu">Users</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/catalogues" class="waves-effect"><i class="fa fa-bookmark fa-fw" aria-hidden="true"></i><span class="hide-menu">Catalogues</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/examples" class="waves-effect"><i class="fa fa-book fa-fw" aria-hidden="true"></i><span class="hide-menu">Examples</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/materials" class="waves-effect"><i class="fa fa-cubes fa-fw" aria-hidden="true"></i><span class="hide-menu">Materials</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/balances" class="waves-effect"><i class="fa fa-cc-visa fa-fw" aria-hidden="true"></i><span class="hide-menu">Account Balances</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/graph" class="waves-effect"><i class="fa fa-pie-chart fa-fw" aria-hidden="true"></i><span class="hide-menu">Cover Graph</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>ceo/city" class="waves-effect"><i class="fa fa-building fa-fw" aria-hidden="true"></i><span class="hide-menu">City</span></a>
           </li>
       </ul>
       <div class="center p-20">
           <span class="hide-menu"><a href="<?php echo base_url(); ?>" target="_blank" class="btn btn-danger btn-block btn-rounded waves-effect waves-light">To Website</a></span>
       </div>
   </div>
</div>
<!-- Left navbar-header end -->
<!-- Page Content -->
