<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Humans</h4></div>
                <ol class="breadcrumb">
                   <li class="active">Filter Level</li>
                   <form action = "<?php echo base_url(); ?>ceo/users" method="POST">
                   <select name="level" id="inputArticle-Sort" class="form-control form-control-line">
                    <option value="">All Level</option>
                    <option value="CEO">CEO</option>
                    <option value="ADMIN">Admin</option>
                    <option value="OPERATOR">Operator</option>
                  </select>
                    <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
						          <i class="fa fa-filter"></i> Filter
                    </button></center>
                </form>
               </ol>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <section id="new_user">
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="40"><center><b>First Name</th>
                                    <th width="40"><center><b>Last Name</th>
                                    <th><center><b>Username</th>
                                    <th><center><b>Address</th>
                                    <th width="30"><center><b>Phone Number</th>
                                    <th width="50"><center><b>Email</th>
                                    <th width="10"><center><b>Level</th>
                                    <th width="200"><center><b>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($user as $users) { ?>
                                <tr>
                                    <td width="40"><center><?php echo $users['nama_depan']; ?></td>
                                    <td width="40"><center><?php echo $users['nama_belakang']; ?></td>
                                    <td><center><?php echo $users['username']; ?></td>
                                    <td><center><?php echo $users['alamat']; ?></td>
                                    <td width="30"><center><?php echo $users['no_hp']; ?></td>
                                    <td width="50"><center><?php echo $users['email']; ?></td>
                                    <td width="10"><center><?php echo $users['level']; ?></td>
                                    <td><center>
                                    <a href="<?php echo site_url('ceo/delete_user/'.$users['id_user']); ?>"class="btn btn-sm btn-danger">Delete</a>
                                    <form action="<?php echo base_url(); ?>ceo/update_user" method="post">
                                      <select name="levelup" class="form-control-line">
                                        <option selected="<?php echo $users['level']; ?>"><?php echo $users['level']; ?></option>
                                        <option value="CEO">CEO</option>
                                        <option value="ADMIN">Admin</option>
                                        <option value="OPERATOR">Operator</option>
                                     </select>
                                   <input type="hidden" name="id_user" value="<?php echo $users['id_user']; ?>">
                                   <button type="submit" name="button" class="btn btn-sm btn-success">Level Up</button>
                                 </form>
                                    </td><?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>

      <?php if (!empty($data_user)){ ?>
     <section id="user">
         <div class="col-md-8 col-xs-12">
             <div class="white-box">
               <?php echo form_open('ceo/update_user/'.$data_user['id_user']); ?>
                     <div class="form-group">
                         <label class="col-md-12">First Name</label>
                         <div class="col-md-12">
                             <input type="text" name="nama_depan" readonly value="<?php echo $data_user['nama_depan'] ?>" placeholder="Johnathan" class="form-control form-control-line" >
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="col-md-12">Last Name</label>
                         <div class="col-md-12">
                             <input type="text" name="nama_belakang" readonly value="<?php echo $data_user['nama_belakang'] ?>" placeholder="Doe" class="form-control form-control-line">
                         </div>
                     </div>

                              <input type="hidden" name="username" value="<?php echo $data_user['username'] ?>" placeholder="johndoe" class="form-control form-control-line">
                              <input type="hidden" name="password" value="<?php echo $data_user['password'] ?>" class="form-control form-control-line">
                              <input type="hidden" name="alamat" value="<?php echo $data_user['alamat'] ?>" class="form-control form-control-line">
                              <input type="hidden" name="no_hp" value="<?php echo $data_user['no_hp'] ?>" placeholder="123 456 7890" class="form-control form-control-line">
                              <input type="hidden" name="email" value="<?php echo $data_user['email'] ?>" placeholder="johnathan@Yaasinqu.com" class="form-control form-control-line" name="example-email" id="example-email">


                     <div class="form-group">
                         <label for="example-email" class="col-md-12">Level</label>
                         <div class="col-md-12">
                             <select name="level" name="level" id="inputArticle-Sort"  value="<?php echo $data_user['level'] ?>" class="form-control form-control-line">
                                 <option selected="<?php echo $data_user['level'] ?>"><?php echo $data_user['level'] ?></option>
                                 <option value="CEO">CEO</option>
                                 <option value="PERSONALIA">PERSONALIA</option>
                                 <option value="ADMIN">ADMIN</option>
                                 <option value="OPERATOR">OPERATOR</option>
                                 <option value="WORKER">WORKER</option>
                             </select>
                         </div>
                     </div>
                     <div class="form-group">
                         <div class="col-sm-12">
                             <button type ="submit" class="btn btn-warning">Update Level</button>
                             <?php echo form_close(); ?>
                         </div>
                     </div>
                 </form>
             </div>
         </div>
     </section>
     <?php } ?>

        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>
</body>

</html>
