<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- panggil favicon !-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-57x57.png">
  	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-60x60.png">
  	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-72x72.png">
  	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-76x76.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-114x114.png">
  	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-120x120.png">
  	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-144x144.png">
  	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-152x152.png">
  	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/img/favicon/apple-touch-icon-180x180.png">
  	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon/favicon-32x32.png" sizes="32x32">
  	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon/favicon-194x194.png" sizes="194x194">
  	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon/favicon-96x96.png" sizes="96x96">
  	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
  	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon/favicon-16x16.png" sizes="16x16">
  	<link rel="manifest" href="<?php echo base_url() ?>assets/img/favicon/manifest.json">
  	<link rel="mask-icon" href="<?php echo base_url() ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
  	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon/favicon.ico">
    <!-- akhir panggil favicon !-->
  	<meta name="msapplication-TileColor" content="#66e0e5">
  	<meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/img/favicon/mstile-144x144.png">
  	<meta name="msapplication-config" content="<?php echo base_url() ?>assets/img/favicon/browserconfig.xml">
  	<meta name="theme-color" content="#ffffff">

    <title>YaasiinQu</title>

    <link href="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>assets/dashboard/html/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/dashboard/html/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>assets/dashboard/html/css/colors/blue-dark.css" id="theme" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <div class="top-left-part"><a class="logo"><b><img src="<?php echo base_url(); ?>assets/img/logo-putih.png" style="height:40px" alt="home" /></b><span class="hidden-xs"></span></a></div>
                <ul class="nav navbar-top-links navbar-left m-l-20 hidden-xs">
                    <li>
                      <!-- <h4 class="page-title">CEO</h4> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <a class="profile-pic" href="<?= base_url(); ?>login/logout"><b class="hidden-xs"><i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>Logout</b> </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
