<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
   <div class="sidebar-nav navbar-collapse slimscrollsidebar">
       <ul class="nav" id="side-menu">
            <li style="padding: 10px 0 0;">
               <a href="<?php echo base_url(); ?>personalia" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i><span class="hide-menu">Profile</span></a>
           </li>
           <li>
               <a href="<?php echo base_url(); ?>personalia/users" class="waves-effect"><i class="fa fa-users fa-fw" aria-hidden="true"></i><span class="hide-menu">Users</span></a>
           </li>
       </ul>
       <div class="center p-20">
           <span class="hide-menu"><a href="<?php echo base_url(); ?>" target="_blank" class="btn btn-danger btn-block btn-rounded waves-effect waves-light">To Website</a></span>
       </div>
   </div>
</div>
<!-- Left navbar-header end -->
<!-- Page Content -->
