<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Humans
                    <button id="add_user" class="btn btn-sm btn-success"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i>New User</button></div>
                  </h4>
                <ol class="breadcrumb">
                   <li class="active">Filter Level</li>
                   <form action = "<?php echo base_url(); ?>personalia/users" method="POST">
                     <select name="level" class="form-control form-control-line">
                      <option value=""></option>
                      <option value="CEO">CEO</option>
                      <option value="PERSONALIA">Personalia</option>
                      <option value="ADMIN">Admin</option>
                      <option value="OPERATOR">Operator</option>
                      <option value="WORKER">Worker</option>
                    </select>
                    <center><button type="submit" id="btn_order" class="btn btn-sm btn-info">
                     <i class="fa fa-filter"></i> Filter
                   </button></center>
                 </form>
               </ol>
             </div>

        <section>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="60"><center><b>First Name</th>
                                    <th width="60"><center><b>Last Name</th>
                                    <th><center><b>Username</th>
                                    <th><center><b>Address</th>
                                    <th><center><b>Phone Number</th>
                                    <th width="40"><center><b>Email</th>
                                    <th width="20"><center><b>Level</th>
                                    <th><center><b>Photo</th>
                                    <th><center><b>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($user as $users) { ?>
                                <tr>
                                    <td width="60"><center><?php echo $users['nama_depan']; ?></td>
                                    <td width="60"><center><?php echo $users['nama_belakang']; ?></td>
                                    <td><center><?php echo $users['username']; ?></td>
                                    <td><center><?php echo $users['alamat']; ?></td>
                                    <td><center><?php echo $users['no_hp']; ?></td>
                                    <td width="40"><center><?php echo $users['email']; ?></td>
                                    <td width="20"><center><?php echo $users['level']; ?></td>
                                    <td><center><img src="<?php echo base_url() ."/assets/img/users/". $users['foto']; ?>" style="height:100px;widht:100px"></center></td>
                                    <td><center>
                                    <a href="<?php echo site_url('personalia/delete_user/'.$users['id_user']); ?>"class="btn btn-sm btn-danger">Hapus</a>
                                    </td><?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </section>

      <section id="form_insert" style="display: none">
          <div class="col-md-8 col-xs-12">
              <div class="white-box">
                  <form enctype="multipart/form-data" class="form-horizontal form-material" action="<?php echo base_url(); ?>personalia/insert_user" method="POST">
                      <div class="form-group">
                          <label class="col-md-12">First Name</label>
                          <div class="col-md-12">
                              <input type="text" required name="nama_depan" placeholder="Johnathan" class="form-control form-control-line" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Last Name</label>
                          <div class="col-md-12">
                              <input type="text" required name="nama_belakang" placeholder="Doe" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Username</label>
                          <div class="col-md-12">
                              <input type="text" required name="username" placeholder="johndoe" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Password</label>
                          <div class="col-md-12">
                              <input type="password" required name="password" class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Address</label>
                          <div class="col-md-12">
                              <textarea rows="5" required name="alamat" class="form-control form-control-line"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-12">Phone No</label>
                          <div class="col-md-12">
                              <input type="text" required name="no_hp" placeholder="123 456 7890" class="form-control form-control-line" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="example-email" class="col-md-12">Email</label>
                          <div class="col-md-12">
                              <input type="email" required name="email" placeholder="johnathan@Yaasinqu.com" class="form-control form-control-line" id="example-email">
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="example-email" class="col-md-12">Level</label>
                          <div class="col-md-12">
                              <select required name="level" id="inputArticle-Sort" class="form-control form-control-line">
                                  <option value="CEO">CEO</option>
                                  <option value="PERSONALIA">Personalia</option>
                                  <option value="ADMIN">Admin</option>
                                  <option value="OPERATOR">Operator</option>
                                  <option value="WORKER">Worker</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="example-email" class="col-md-12">Photo</label>
                          <div class="col-md-12">
                              <input type="file" required name="foto"class="form-control form-control-line">
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                              <button type ="submit" class="btn btn-success">Create New Profile</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
      </section>


        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#add_user').on('click', function() {
      $('#form_insert').attr('style', "display: 'block'");
    })
  })
</script>

</body>

</html>
