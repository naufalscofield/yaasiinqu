        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Orders</h4> </div>
                        <ol class="breadcrumb">
                           </ol>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="table-responsive">
                                <table class="table">
                                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                      <h4 class="page-title">Click on the Photo to get real photo and to download it</h4> </div>
                                    <thead>
                                        <tr>
                                            <th><center><b>Order Code</th>
                                            <th><center><b>Buyer</th>
                                            <th><center><b>Phone Number</th>
                                            <th><center><b>City</th>
                                            <th><center><b>Address</th>
                                            <th><center><b>Alamarhum Name</th>
                                            <th><center><b>Birth Place</th>
                                            <th><center><b>Birth Date</th>
                                            <th><center><b>Death Place</th>
                                            <th><center><b>Death Date</th>
                                            <th><center><b>Binti</th>
                                            <th><center><b>Family</th>
                                            <th><center><b>Design Code</th>
                                            <th><center><b>Color</th>
                                            <th><center><b>Photo</th>
                                            <th><center><b>Amount</th>
                                            <th><center><b>Notes</th>
                                            <th><center><b>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($orderan as $orderans) { ?>
                                        <tr>
                                            <td><?php echo $orderans['kode_order']; ?></td>
                                            <td><?php echo $orderans['pengorder']; ?></td>
                                            <td><?php echo $orderans['no_telp']; ?></td>
                                            <td><?php echo $orderans['kota']; ?></td>
                                            <td><?php echo $orderans['alamat']; ?></td>
                                            <td><?php echo $orderans['nama_alm']; ?></td>
                                            <td><?php echo $orderans['tempat_lahir']; ?></td>
                                            <td><?php echo $orderans['tanggal_lahir']; ?></td>
                                            <td><?php echo $orderans['tempat_wafat']; ?></td>
                                            <td><?php echo $orderans['tanggal_wafat']; ?></td>
                                            <td><?php echo $orderans['binti']; ?></td>
                                            <td><?php echo $orderans['keluarga']; ?></td>
                                            <td><?php echo $orderans['kode_desain']; ?></td>
                                            <td><?php echo $orderans['warna']; ?></td>
                                            <td>
                                              <a href = "<?php echo base_url() ."assets/img/order/". $orderans['foto']; ?>"><img src="<?php echo base_url() ."assets/img/order/". $orderans['foto']; ?>" style="width:100px; height:100px"></a>
                                            </td>
                                            <td><?php echo $orderans['jumlah']; ?></td>
                                            <td><?php echo $orderans['catatan']; ?></td>
                                            <td><center>
                                            <a href="<?php echo site_url('worker/update_order/'.$orderans['id_order']); ?>"class="btn btn-sm btn-info">Update Status</a>
                                              </td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Pixel Admin brought to you by wrappixel.com </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/dashboard/html/js/custom.min.js"></script>
</body>

</html>
