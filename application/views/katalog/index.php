<!-- END SECTION: Stats -->
	<!-- SECTION: Articles -->
	<section class="latest-articles has-padding alternate-bg" id="katalog">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<h4>Katalog</h4>
				</div>
				<div class="col-md-8 col-sm-8 sort">
					<form action = "<?php echo base_url(); ?>katalog" method="POST">
					<select name="cover" id="inputArticle-Sort" class="">
						<option value="">Semua Cover</option>
						<option value="3">Cover Blurdru</option>
						<option value="2">Hard Cover</option>
						<option value="1">Soft Cover</option>
					</select>
					<button type="submit" id="btn_order" class="btn primary">
						<i class="fa fa-filter"></i> Filter
					</button>
				</form>
				</div>
			</div>
		</div>
			<div class="row">
				<div class="col-md-4">
					<article class="article-post">
							<figure>
								<?php foreach ($katalog as $katalog_key) { ?>
							<figcaption>
										<center><h2>Model <?php echo $katalog_key['kode_desain']; ?></h2></center><br>
											<img src="<?php echo base_url() ."/assets/img/katalog/". $katalog_key['foto']; ?>" style="height:200px;widht:200px"></center><br>
								 			<p><li><?php echo $katalog_key['jenis_cover']; ?></li><br>
                      <li><?php echo $katalog_key['jumlah_lembar']; ?></li><br>
                      <li><?php echo $katalog_key['jenis_kertas']; ?></li><br>
                      <li><?php echo $katalog_key['harga']; ?></li><br>
											<form action="<?php echo base_url(); ?>katalog/order" method="POST">
												<div class="form-group">
													<center>
														<input type="hidden" name="harga" value="<?php echo $katalog_key['harga'] ?>">
														<input type="hidden" name="id_desain" value="<?php echo $katalog_key['id_desain'] ?>">
														<input type="hidden" name="kode_desain" value="<?php echo $katalog_key['kode_desain'] ?>">
														<input type="hidden" name="id_cover" value="<?php echo $katalog_key['id_cover'] ?>">
														<input type="hidden" name="foto" value="<?php echo $katalog_key['foto'] ?>">
														<button type="submit" class="btn primary ">Saya Pesan Yang Ini</button>
													</div>
												</form>
							</figcaption>
								<?php } ?>
								</figure>
					</article>
				</div>
		</div>
</section>
</div>
	<!-- END SECTION: Articles -->
