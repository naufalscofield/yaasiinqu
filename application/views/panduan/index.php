<title>YaasiinQu</title>
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/manifest.json">
<link rel="mask-icon" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/bootstrap/img/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#66e0e5">
<meta name="msapplication-TileImage" content="<?php echo base_url() ?>assets/bootstrap/img/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="<?php echo base_url() ?>assets/bootstrap/img/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<!-- end favicon links -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/normalize.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/flickity.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/styles.css">
<!-- ijal next -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap-datepicker.min.css" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js"></script>
<script src="http://vjs.zencdn.net/5.4.6/video.min.js"></script>
<!-- SECTION: Promo -->
<!-- <br>&nbsp<a href="<?php echo base_url(); ?>">Kembali</a> -->
<section class="collective has-padding" id="intro">
  <div class="container">
    <h1><a href="<?php echo base_url();?>"><img src="<?php echo base_url() ?>assets/img/logo.png" alt="YaasiinQu-Logo" style="height:50px;height:50px">Panduan Pembeli</a></h1>
      <br>Panduan Pembelian Buku Yaasiin di YaasiinQu
          Pesan buku yaasiin di YaasiinQu itu mudah dan aman. Transaksi dijamin aman. Dana transaksi dikembalikan 100% jika barang tidak dikirim.<br>
      <br><div class="col-md-12 wp3">
          <img src="<?php echo base_url() ?>assets/img/panduan/11.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <img src="<?php echo base_url() ?>assets/img/panduan/22.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <img src="<?php echo base_url() ?>assets/img/panduan/33.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <img src="<?php echo base_url() ?>assets/img/panduan/44.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <img src="<?php echo base_url() ?>assets/img/panduan/55.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <img src="<?php echo base_url() ?>assets/img/panduan/66.png" alt="YaasiinQu-Logo" style="height:310px;height:310px">
          <div class="col-md-10">
          </div>
        </div>
  </div>
</section>
<!-- END SECTION: Promo -->
