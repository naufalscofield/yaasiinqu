<!-- SECTION: Crew -->
<section class="crew has-padding alternate-bg" id="team">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Testimoni</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <article class="crew-member" style="background-image: url(<?php echo base_url() ?>assets/bootstrap/img/yaasiin1.jpg)">
        </article>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <article class="crew-member" style="background-image: url(<?php echo base_url() ?>assets/bootstrap/img/yaasiin2.jpg)">
        </article>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <article class="crew-member" style="background-image: url(<?php echo base_url() ?>assets/bootstrap/img/yaasiin3.jpg)">
        </article>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <article class="crew-member" style="background-image: url(<?php echo base_url() ?>assets/bootstrap/img/yaasiin5.jpg)">
          <figure><br>
        </article><br>
      </div>
</section>
</div>
<!-- END SECTION: Crew -->
