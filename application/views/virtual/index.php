
<section class="collective has-padding" id="">

  <!--buat progress bar-->
  <center>
        <h3> Dengarkan Yaasiin </h3><br>
        <div id="kotak" style="border: 2px solid greenyellow; height: 20px; width: 1000px; margin-bottom: 10px">
             <div id="progres" style="background: greenyellow; height: 17px; width: 0px"></div>
         </div>
         <!--tambahkan element audio-->
         <audio id="yaasiin" src="<?php echo base_url(); ?>assets/music/yaasiin.mp3"></audio>

         <!--tambahkan tombol-->
         <div class="form-group">
         <button class="btn primary" id="tombol_pause" onclick="pause()"><i class="fa fa-pause"></i></button>
         <button class="btn primary" id="tombol_play" onclick="play()"><i class="fa fa-play"></i></button>
         <button class="btn primary" id="tombol_stop" onclick="stop()"><i class="fa fa-stop"></i></button>
       </div>
         <script>
  //            definisikan masing masing id
             var lagu = document.getElementById('yaasiin');
             var tombol_play = document.getElementById('tombol_play');
             var tombol_pause = document.getElementById('tombol_pause');
             var tombol_stop = document.getElementById('tombol_stop');
             var progres = document.getElementById('progres');

  //            set tombol
             tombol_play.disabled = false;
             tombol_pause.disabled = true;
             tombol_stop.disabled = true;

             function play() {
  //                harviacode.com
                 lagu.play();
                 tombol_play.disabled = true;
                 tombol_pause.disabled = false;
                 tombol_stop.disabled = false;
                 update();
             }

             function pause() {
  //                harviacode.com
                 lagu.pause();
                 tombol_play.disabled = false;
                 tombol_pause.disabled = true;
                 tombol_stop.disabled = false;
                 update();
             }

             function stop() {
  //                harviacode.com
                 lagu.pause();
                 lagu.currentTime = 0;
                 tombol_play.disabled = false;
                 tombol_pause.disabled = true;
                 tombol_stop.disabled = true;
                 update();
             }

             function update() {
  //                update progress tiap 200 milidetik
                 setInterval(function () {
  //                harviacode.com
                     // cari posisi lagu dan durasi
                     var saatini = lagu.currentTime;
                     var durasi = lagu.duration;

  //                    hitung persentase progress
                     var persen = (saatini / durasi) * 1000;

                     // update progress bar
                     progres.style.width = parseInt(persen) + 'px';
                 }, 200);
             }

         </script></center><br>
  <br><div class="container">
      <div class="col-md-12 text-center">
        <table class="table">
            <thead>
                <tr>
                    <th><font face="arial" size=4><center><b>Ayat</b></th>
                    <th><font face="arial" size=4><center><b>Terjemah</b></th>
                    <th><font face="arial" size=4><center><b>Latin</b></th>
                    <th><font face="arial" size=4><center><b>Arab</b></th>
                </tr>
            </thead>
            <tbody>
              <?php
              header('Content-type: text/html; charset=utf-8');
               foreach ($yasin as $yasins) { ?>
                <tr>
                    <td><font face="arial" size=3><?php echo $yasins['ayat']; ?></td>
                    <td><font face="arial" size=3><?php echo $yasins['terjemahan']; ?></td>
                    <td><font face="arial" size=3><?php echo $yasins['latin']; ?></td>
                    <td><p align="right"><font face="arial" size=18 color="green"><?php echo $yasins['arab']; ?></td>
                    <?php } ?>
                </tr>
            </tbody>
          </table>
    </div>
  </div>
</div>
</section>
