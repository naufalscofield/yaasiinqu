-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 10 Jan 2018 pada 07.05
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yaasiinqu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_cover`
--

CREATE TABLE `tb_cover` (
  `id_cover` int(11) NOT NULL,
  `jenis_cover` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_cover`
--

INSERT INTO `tb_cover` (`id_cover`, `jenis_cover`) VALUES
(1, 'Soft Cover'),
(2, 'Hard Cover'),
(3, 'Blurdru Cover');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_hasil`
--

CREATE TABLE `tb_hasil` (
  `id_hasil` int(11) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_hasil`
--

INSERT INTO `tb_hasil` (`id_hasil`, `foto`) VALUES
(1, 'yaasiin1.jpg'),
(2, 'yaasiin3.jpg'),
(3, 'yaasiin5.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_katalog`
--

CREATE TABLE `tb_katalog` (
  `id_desain` int(11) NOT NULL,
  `kode_desain` varchar(20) NOT NULL,
  `foto` text NOT NULL,
  `id_cover` int(11) NOT NULL,
  `id_lembar` int(11) NOT NULL,
  `id_kertas` int(11) NOT NULL,
  `warna` varchar(100) NOT NULL,
  `harga` varchar(20) NOT NULL,
  `status` enum('ready','oos') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_katalog`
--

INSERT INTO `tb_katalog` (`id_desain`, `kode_desain`, `foto`, `id_cover`, `id_lembar`, `id_kertas`, `warna`, `harga`, `status`) VALUES
(1, 'SC01', '13.png', 1, 1, 1, 'merah,emas,coklat', '10000', 'ready'),
(2, 'HC01', '2.png', 2, 2, 1, 'merah,kuning,hijau', '15000', 'ready'),
(3, 'BD01', '3.png', 3, 3, 1, 'silver,emas,biru', '18000', 'ready'),
(4, 'SC02', '4.png', 1, 1, 1, 'merah,kuning,biru,hijau', '14000', 'ready');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kertas`
--

CREATE TABLE `tb_kertas` (
  `id_kertas` int(11) NOT NULL,
  `jenis_kertas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kertas`
--

INSERT INTO `tb_kertas` (`id_kertas`, `jenis_kertas`) VALUES
(1, 'HVS'),
(3, 'Art Paper');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kota`
--

CREATE TABLE `tb_kota` (
  `id_kota` int(11) NOT NULL,
  `nama_kota` varchar(50) NOT NULL,
  `ongkir` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kota`
--

INSERT INTO `tb_kota` (`id_kota`, `nama_kota`, `ongkir`) VALUES
(1, 'Bandung', '8000'),
(2, 'Jakarta', '11000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_lembar`
--

CREATE TABLE `tb_lembar` (
  `id_lembar` int(11) NOT NULL,
  `jumlah_lembar` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_lembar`
--

INSERT INTO `tb_lembar` (`id_lembar`, `jumlah_lembar`) VALUES
(1, 180),
(2, 200),
(3, 280),
(5, 380);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_order`
--

CREATE TABLE `tb_order` (
  `id_order` int(11) NOT NULL,
  `kode_order` varchar(20) NOT NULL,
  `tgl_order` date NOT NULL,
  `pengorder` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `id_kota` int(3) NOT NULL,
  `alamat` text NOT NULL,
  `nama_alm` varchar(100) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_wafat` varchar(50) NOT NULL,
  `tanggal_wafat` date NOT NULL,
  `binti` varchar(100) NOT NULL,
  `keluarga` text NOT NULL,
  `kode_desain` varchar(10) NOT NULL,
  `id_cover` int(11) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `id_bank` int(11) NOT NULL,
  `total` varchar(50) NOT NULL,
  `bukti_transaksi` text NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_order`
--

INSERT INTO `tb_order` (`id_order`, `kode_order`, `tgl_order`, `pengorder`, `no_telp`, `id_kota`, `alamat`, `nama_alm`, `tempat_lahir`, `tanggal_lahir`, `tempat_wafat`, `tanggal_wafat`, `binti`, `keluarga`, `kode_desain`, `id_cover`, `warna`, `foto`, `jumlah`, `catatan`, `id_bank`, `total`, `bukti_transaksi`, `status`) VALUES
(1, '679081234567890', '2018-01-10', 'Sansa Stark', '081234567890', 1, 'Winterfell Street,The North', 'Eddard Stark', 'The North', '2018-01-02', 'Kingslanding', '2018-01-31', 'Rickard Stark', 'Sansa Stark,Arya Stark', 'SC01', 1, 'coklat', 'EddardStark1.jpg', 20, 'Please fast', 1, '208679', 's__2007162.jpg', 'done'),
(2, '899081234567890', '2018-01-10', 'Clay Jensen', '081234567890', 2, 'Liberty Street', 'Hannah Baker', 'New York', '2018-01-23', 'Toronto', '2018-01-31', 'Brandon Baker', 'Brandon Baker,Clay Jensen', 'SC01', 1, 'merah', 'Katherine_Langford_as_Hannah_Baker.jpg', 20, 'Be Fast', 2, '211899', 's__20071621.jpg', 'proccess'),
(3, '936081234567890', '2018-01-10', 'Gillian Foster', '081234567890', 1, 'Saint Johnson Street', 'Cal Lightman', 'Washington', '2018-01-09', 'Washington', '2018-01-31', 'Edward Lightman', 'Gillian Foster,Emily Lightman', 'SC02', 1, 'merah', 'Lightman_1.jpg', 20, 'Fast :)', 2, '291936', 's__20071622.jpg', 'paid'),
(4, '882081234567890', '2018-01-10', 'Kate Austen', '081234567890', 2, 'Lost Island Street', 'Jack Shepard', 'New York', '2018-01-17', 'NTT', '2018-01-25', 'Christian Shepard', 'Kate Austen,Hurley', 'HC01', 2, 'hijau', 'Jack-jack-shephard-6831861-1024-768.jpg', 20, '', 1, '308882', 's__20071623.jpg', 'paid'),
(5, '555081234567890', '2018-01-10', 'Michael Scofield', '081234567890', 1, 'Fox River Street', 'Sarah Tancredi', 'Alburqueque', '2018-01-12', 'Mexico City', '2018-01-30', 'Frank Tancredi', 'Michael Scofield,Lincold Burrows', 'HC01', 2, 'merah', '220px-SaraTancredi1.jpg', 20, 'I want it next week please', 1, '308555', '', 'waiting'),
(6, '546081234567890', '2018-01-10', 'Sherlock Holmes', '081234567890', 2, 'Baker street 2212', 'Irene Adler', 'London', '2018-01-10', 'London', '2018-01-31', 'Kevan Adler', 'Sherlock Holmes,Kevan Adler', 'BD01', 3, 'silver', 'irene_adler___painting_by_lasse17-d50opdk.png', 20, 'Please fast', 2, '371546', '', 'waiting');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_payment`
--

CREATE TABLE `tb_payment` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `no_rek` varchar(25) NOT NULL,
  `atas_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_payment`
--

INSERT INTO `tb_payment` (`id_bank`, `nama_bank`, `no_rek`, `atas_nama`) VALUES
(1, 'BNI', '1000', 'yaasiinqu'),
(2, 'BRI', '2000', 'yaasiinqu'),
(3, 'MANDIRI', '3000', 'yaasiinqu'),
(5, 'BJB', '4000', 'yaasiinqu'),
(6, 'RABOBANK', '5000', 'yaasiinqu'),
(7, 'WOORI SAUDARA', '6000', 'yaasiinqu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` enum('CEO','ADMIN','OPERATOR') NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_depan`, `nama_belakang`, `username`, `password`, `alamat`, `no_hp`, `email`, `level`, `foto`) VALUES
(1, 'Riandaka', 'Rizal Hikmah Romadhon', 'riandaka20', '22f3b973ab47bfa9d6f19e1aea494aa8', 'Jln.Moh Toha', '081234567890', 'riandaka20@gmail.com', 'ADMIN', 'rizal.jpg'),
(2, 'Naufal', 'Ramadhan', 'scofield', 'd8ae14c9f94bbb60524438de26d86204', 'Jln.Sukarasa', '085608571662', 'ramnaufal@gmail.com', 'CEO', 'naufal4.jpg'),
(3, 'Farhan', 'Maulana', 'farhanm', 'a904bd30ebce1dc9d064aa23a7b0c11a', 'Jln.Pasir Impun', '081234567890', 'farhanm@gmail.com', 'OPERATOR', 'farhan2.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_yasin`
--

CREATE TABLE `tb_yasin` (
  `ayat` int(11) NOT NULL,
  `terjemahan` varchar(200) CHARACTER SET utf8 NOT NULL,
  `latin` varchar(200) CHARACTER SET utf8 NOT NULL,
  `arab` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_yasin`
--

INSERT INTO `tb_yasin` (`ayat`, `terjemahan`, `latin`, `arab`) VALUES
(1, 'Yaa Siin.', 'Yaa-Siin', 'يس'),
(2, 'Demi Al Qur''an yang penuh hikmah,', 'Wal-Qur-aanil-Hakiim', 'وَالْقُرْآنِ الْحَكِيمِ'),
(3, 'sesungguhnya kamu salah seorang dari rasul-rasul,', 'Innaka laminal mursaliin', 'إِنَّكَ لَمِنَ الْمُرْسَلِينَ'),
(4, '(yang berada) di atas jalan yang lurus,', 'Alaa Siraatim Mustaqiim', 'عَلَى صِرَاطٍ مُسْتَقِيمٍ	'),
(5, '(sebagai wahyu) yang diturunkan oleh Yang Maha Perkasa lagi Maha Penyayang.', 'Tanziilal ''Aziizir Rohiim', 'تَنْزِيلَ الْعَزِيزِ الرَّحِيمِ'),
(6, 'agar kamu memberi peringatan kepada kaum yang bapak-bapak mereka belum pernah diberi peringatan, karena itu mereka lalai.', 'Litundziro qowmam maaa undziro aabaaa''uhum fahum ghoofiluun', 'لِتُنْذِرَ قَوْمًا مَا أُنْذِرَ آبَاؤُهُمْ فَهُمْ غَافِلُونَ'),
(7, 'Sesungguhnya telah pasti berlaku perkataan (ketentuan Allah) terhadap kebanyakan mereka, karena mereka tidak beriman.', 'Laqod haqqol qowlu ''alaaa aksarihim fahum la yu''minuun', 'لَقَدْ حَقَّ الْقَوْلُ عَلَى أَكْثَرِهِمْ فَهُمْ لا يُؤْمِنُونَ	'),
(8, 'Sesungguhnya Kami telah memasang belenggu di leher mereka, lalu tangan mereka (diangkat) ke dagu, maka karena itu mereka tertengadah.', 'Innaa ja''alnaa fiii a''naaqihim aghlaalan fahiya ilal adzqooni fahum muqmahuun', 'إِنَّا جَعَلْنَا فِي أَعْنَاقِهِمْ أَغْلالا فَهِيَ إِلَى الأذْقَانِ فَهُمْ مُقْمَحُونَ'),
(9, 'Dan Kami adakan di hadapan mereka dinding dan di belakang mereka dinding (pula), dan Kami tutup (mata) mereka sehingga mereka tidak dapat melihat.', 'Wa ja''alnaa mim baini aydiihim saddaw-wa min kholfihim saddan fa aghshay naahum fahum laa yubsiruun', 'وَجَعَلْنَا مِنْ بَيْنِ أَيْدِيهِمْ سَدًّا وَمِنْ خَلْفِهِمْ سَدًّا فَأَغْشَيْنَاهُمْ فَهُمْ لا يُبْصِرُونَ	'),
(10, 'Sama saja bagi mereka apakah kamu memberi peringatan kepada mereka ataukah kamu tidak memberi peringatan kepada mereka, mereka tidak akan beriman.', 'Wa sawaaa''un ''alayhim ''a-andzartahum am lam tundzirhum laa yu''minuun', 'وَسَوَاءٌ عَلَيْهِمْ أَأَنْذَرْتَهُمْ أَمْ لَمْ تُنْذِرْهُمْ لا يُؤْمِنُونَ'),
(11, 'Sesungguhnya kamu hanya memberi peringatan kepada orang-orang yang mau mengikuti peringatan dan yang takut kepada Tuhan Yang Maha Pemurah walaupun dia tidak melihat-Nya. Maka berilah mereka kabar gemb', 'Innamaa tundziru manit taba ''az-Dzikro wa khoshiyar Rahmaana bilghaib, fabashshirhu bimaghfirotiw-wa ajrin kariim', 'إِنَّمَا تُنْذِرُ مَنِ اتَّبَعَ الذِّكْرَ وَخَشِيَ الرَّحْمَنَ بِالْغَيْبِ فَبَشِّرْهُ بِمَغْفِرَةٍ وَأَجْرٍ كَرِيمٍ	'),
(12, 'Sesungguhnya Kami menghidupkan orang-orang mati dan Kami menuliskan apa yang telah mereka kerjakan dan bekas-bekas yang mereka tinggalkan. Dan segala sesuatu Kami kumpulkan dalam Kitab Induk yang nyat', 'Innaa Nahnu nuhyil mawtaa wa naktubu maa qoddamuu wa aasaarohum; wa kulla shay''in ahsainaahu fiii Imaamim Mubiin', 'إِنَّا نَحْنُ نُحْيِي الْمَوْتَى وَنَكْتُبُ مَا قَدَّمُوا وَآثَارَهُمْ وَكُلَّ شَيْءٍ أحْصَيْنَاهُ فِي إِمَامٍ مُبِينٍ'),
(13, 'Dan buatlah bagi mereka suatu perumpamaan, yaitu penduduk suatu negeri ketika utusan-utusan datang kepada mereka;', 'Wadrib lahum masalan Ashaabal Qoryatih; iz jaaa''ahal mursaluun', 'وَاضْرِبْ لَهُمْ مَثَلا أَصْحَابَ الْقَرْيَةِ إِذْ جَاءَهَا الْمُرْسَلُونَ	'),
(14, '(yaitu) ketika Kami mengutus kepada mereka dua orang utusan, lalu mereka mendustakan keduanya; kemudian Kami kuatkan dengan (utusan) yang ketiga, maka ketiga utusan itu berkata: "Sesungguhnya kami ada', 'Idz arsalnaaa ilaihimutsnaini fakadzzabuuhumaa fa''azzaznaa bisaalisin faqooluu innaaa ilaikum mursaloon', 'إِذْ أَرْسَلْنَا إِلَيْهِمُ اثْنَيْنِ فَكَذَّبُوهُمَا فَعَزَّزْنَا بِثَالِثٍ فَقَالُوا إِنَّا إِلَيْكُمْ مُرْسَلُونَ	'),
(15, 'Mereka menjawab: "Kamu tidak lain hanyalah manusia seperti kami dan Allah Yang Maha Pemurah tidak menurunkan sesuatu pun, kamu tidak lain hanyalah pendusta belaka".', 'Qooluu maaa antum illaa basharum mislunaa wa maaa anzalar Rohmaanu min shai''in in antum illaa takdzibuun', 'قَالُوا مَا أَنْتُمْ إِلا بَشَرٌ مِثْلُنَا وَمَا أَنْزَلَ الرَّحْمَنُ مِنْ شَيْءٍ إِنْ أَنْتُمْ إِلا تَكْذِبُونَ'),
(16, 'Mereka berkata: "Tuhan kami mengetahui bahwa sesungguhnya kami adalah orang yang diutus kepada kamu.', 'Qooluu Robbunaa ya''lamu innaaa ilaikum lamursaluun', 'قَالُوا رَبُّنَا يَعْلَمُ إِنَّا إِلَيْكُمْ لَمُرْسَلُونَ	'),
(17, 'Dan kewajiban kami tidak lain hanyalah menyampaikan (perintah Allah) dengan jelas".', 'Wa maa ''alainaaa illal balaaghul mubiin', 'وَمَا عَلَيْنَا إِلا الْبَلاغُ الْمُبِينُ	'),
(18, 'Mereka menjawab: "Sesungguhnya kami bernasib malang karena kamu, sesungguhnya jika kamu tidak berhenti (menyeru kami), niscaya kami akan merajam kamu dan kamu pasti akan mendapat siksa yang pedih dari', 'Qooluu innaa tataiyarnaa bikum la''il-lam tantahuu lanar jumannakum wa la-yamassan nakum minna ''adzaabun aleem', 'قَالُوا إِنَّا تَطَيَّرْنَا بِكُمْ لَئِنْ لَمْ تَنْتَهُوا لَنَرْجُمَنَّكُمْ وَلَيَمَسَّنَّكُمْ مِنَّا عَذَابٌ أَلِيمٌ'),
(19, 'Utusan-utusan itu berkata: "Kemalangan kamu itu adalah karena kamu sendiri. Apakah jika kamu diberi peringatan (kamu mengancam kami)? Sebenarnya kamu adalah kaum yang melampaui batas".', 'Qooluu tooo''irukum ma''akum; a''in zukkirtum; bal antum qowmum musrifuun', 'قَالُوا طَائِرُكُمْ مَعَكُمْ أَئِنْ ذُكِّرْتُمْ بَلْ أَنْتُمْ قَوْمٌ مُسْرِفُونَ'),
(20, 'Dan datanglah dari ujung kota, seorang laki-laki (Habib An Najjar) dengan bergegas-gegas ia berkata: "Hai kaumku, ikutilah utusan-utusan itu,', 'Wa jaaa''a min aqsal madiinati rojuluny yas''aa qoola yaa qowmit tabi''ul mursaliin', 'وَجَاءَ مِنْ أَقْصَى الْمَدِينَةِ رَجُلٌ يَسْعَى قَالَ يَا قَوْمِ اتَّبِعُوا الْمُرْسَلِينَ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_cover`
--
ALTER TABLE `tb_cover`
  ADD PRIMARY KEY (`id_cover`);

--
-- Indexes for table `tb_hasil`
--
ALTER TABLE `tb_hasil`
  ADD PRIMARY KEY (`id_hasil`);

--
-- Indexes for table `tb_katalog`
--
ALTER TABLE `tb_katalog`
  ADD PRIMARY KEY (`id_desain`);

--
-- Indexes for table `tb_kertas`
--
ALTER TABLE `tb_kertas`
  ADD PRIMARY KEY (`id_kertas`);

--
-- Indexes for table `tb_kota`
--
ALTER TABLE `tb_kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `tb_lembar`
--
ALTER TABLE `tb_lembar`
  ADD PRIMARY KEY (`id_lembar`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `tb_payment`
--
ALTER TABLE `tb_payment`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tb_yasin`
--
ALTER TABLE `tb_yasin`
  ADD PRIMARY KEY (`ayat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_cover`
--
ALTER TABLE `tb_cover`
  MODIFY `id_cover` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_hasil`
--
ALTER TABLE `tb_hasil`
  MODIFY `id_hasil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_katalog`
--
ALTER TABLE `tb_katalog`
  MODIFY `id_desain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_kertas`
--
ALTER TABLE `tb_kertas`
  MODIFY `id_kertas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kota`
--
ALTER TABLE `tb_kota`
  MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_lembar`
--
ALTER TABLE `tb_lembar`
  MODIFY `id_lembar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_payment`
--
ALTER TABLE `tb_payment`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_yasin`
--
ALTER TABLE `tb_yasin`
  MODIFY `ayat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
